<?php
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params'      => [
                    'host'          => 'localhost',
                    'port'          => '3306',
                    'user'          => 'root',
                    'password'      => '',
                    'dbname'        => 'interactive_talk',
                    'driverOptions' => [
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
                    ]
                ]
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'string_functions' => [
                    #'LPAD' => 'Base\Dql\LpadFunction'
                ],
                'metadata_cache' => 'apc',
                'query_cache' => 'apc',
                'result_cache' => 'apc',
                'generate_proxies' => false,
                'hydration_cache' => 'apc'
            ]
        ],
        'driver' => [
            'app.driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../module/App/src/App/Entity',
                    __DIR__ . '/../../module/Admin/src/Admin/Entity'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    'App\Entity' => 'app.driver',
                    'Admin\Entity' => 'app.driver'
                ]
            ]
        ]
    ]
];