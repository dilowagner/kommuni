<?php
return array(
    'module_layouts' => array(
        'Admin'   => 'layout/admin'
    ),
    'navigation' => array(
        'menu_admin' => array(
            'inicio' => array(
                'label' => 'Início',
                'class' => 'fa fa-home',
                'route' => 'admin-index',
                'action' => 'index'
            ),
            'presentation' => array(
                'label' => 'Apresentações',
                'class' => 'fa fa-folder-o',
                'route' => 'admin-presentation',
                'action' => 'index'
            ),
            'statistics' => array(
                'label' => 'Estatísticas',
                'class' => 'fa fa-bar-chart',
                'route' => 'admin-statistics',
                'action' => 'index'
            ),
            /*'usuarios' => array(
                'label' => 'Usuários',
                'class' => 'fa fa-users',
                'route' => 'admin-user',
                'action' => 'index'
            ),*/
            'help' => array(
                'label' => 'Ajuda',
                'class' => 'fa fa-question-circle',
                'route' => 'admin-help',
                'action' => 'index'
            ),
        )
    ),
);

