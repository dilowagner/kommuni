var presentation = (function () {
    // public
    return {
        validate: function() {
            var name = $('#presentation-title').val();
            if (name === '' || name.length < 3) {
                modal.printMessage('Informe o título da apresentação!<br />Deve conter no mínimo 3 caracteres.', 'error');
                return false;
            }
            return true;
        },
        validateStart: function() {
            var local = $('#presentation-local').val();
            if (local === '' || local.length < 3) {
                modal.printMessage('Informe o local onde será realizada a apresentação!<br />Deve conter no mínimo 3 caracteres.', 'error');
                return false;
            }
            return true;
        },
        save: function() {
            $.ajax({
                url: '/admin/presentation/save',
                data: 'name='+$('#presentation-title').val(),
                type: 'POST',
                beforeSend: function() {
                    modal.disable();
                    modal.clearMessage();
                },
                success: function(result) {
                    if (result.status && result.status === 'success' && result.id) {
                        modal.printMessage('Apresentação criada com sucesso!<br /><i class="fa fa-spinner fa-pulse"></i>&nbsp;Redirecionando...', 'success');
                        window.location.href = '/admin/presentation/edit/'+result.id;
                    } else if (result.status && result.status === 'error' ) {
                        modal.enable();
                        modal.printMessage(result.message, 'error');
                    } else {
                        modal.enable();
                        modal.printMessage('Sem resposta da operação!', 'error');
                    }
                },
                error: function(e){
                    modal.enable();
                    modal.printMessage('Sem resposta da operação!', 'error');
                }
            });
        },
        start: function() {

            var user   = $('#presentation-user').val();
            var token  = $('#presentation-token').val();
            var local  = $('#presentation-local').val();
            var amount = $('#presentation-amount-people').val();

            $.ajax({
                url: '/presentation/start',
                data: {
                    user: user,
                    token: token,
                    local: local,
                    amountPeople: amount
                },
                type: 'POST',
                beforeSend: function() {
                    modal.disable();
                    modal.clearMessage();
                    modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp; Processando...', 'success');

                    setTimeout(function(){
                        modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp; Aguarde, Gerando PDF...', 'success');
                    }, 3000);
                },
                success: function(result) {
                    if (result.status && result.status === 'success') {
                        modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp;Redirecionando...', 'success');
                        window.location.href = '/presentation';
                    } else if (result.status && result.status === 'error' ) {
                        modal.enable();
                        modal.printMessage(result.message, 'error');
                    } else {
                        modal.enable();
                        modal.printMessage('Sem resposta da operação!', 'error');
                    }
                },
                error: function(e){
                    modal.enable();
                    modal.printMessage('Sem resposta da operação!', 'error');
                }
            });
        }
    }
}());

var modal = (function () {
    // public
    return {
        clearMessage: function() {
            $('.modal .modal-body .alert').remove();
        },
        printMessage: function(message, type) {
            modal.clearMessage();
            $('.modal .modal-body').prepend('<div class="alert alert-'+ type +'">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message + '</div>');
        },
        disable: function() {
            $('.modal button, input').prop("disabled",true);
        },
        enable: function() {
            $('.modal button, input').prop("disabled",false);
        }
    }
}());

$(document).ready( function() {

    $("#bt-new").on('click', function() {
        modal.clearMessage();
        modal.enable();
        $('#presentation-title').val('').focus();
    });

    $("#bt-save").on('click', function() {
        if (presentation.validate() && presentation.save()) {
            $('#modal-new').modal('hide');
        }
    });

    // DELETE
    $('a.delete').click(function(e) {
        e.preventDefault();

        var msg = "Deseja realmente excluir a apresentação <b>" + $(this).data('id') + "</b> ?";
        var url = $(this).data('url');

        $.msgBox({
            title: "Excluir?",
            content: msg,
            type: "confirm",
            buttons: [{ value: "Sim" }, { value: "Não" }],
            success: function (result) {
                if (result == "Sim") {
                    window.location.href = url;
                }
            }
        });
    });

    $("#presentation-amount-people").mask('9?9999');
    $("a.start-presentation").on('click', function() {
        var token = $(this).data('token');
        if($(this).hasClass('btn-runnable')) {
            window.location.href = '/presentation/restart/' + token;
            return false;
        }
        modal.clearMessage();
        modal.enable();
        $('#presentation-local').val('').focus();
        $('#presentation-token').val(token);
    });

    $("#bt-start").on('click', function() {
        if (presentation.validateStart() && presentation.start()) {
            $('#modal-start').modal('hide');
        }
    });

});
