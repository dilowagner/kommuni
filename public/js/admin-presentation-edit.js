// TODO: ao inserir slide scroll bottom
// TODO: opção da resposta da interacao no proximo slide
// TODO: enviar PDF por email ou redes sociais
// TODO: espaçamento das linhas

var insertImage = 'SLIDE'; // ELEMENT

/**#################### SLIDE #########################
 * Métodos e propriedades para manipular o slide
 * ####################################################
 */
var slide = (function () {
    // private
    var defaultProperties = {
        'style' : {
            'background-color': '#ffffff',
            'background-image': 'none'
        },
        'class' : 'common transition-none',
        'data-transition' : 'convex',
        'data-background' : '#ffffff'
    };

    // public
    return {

        getProperties : function(selected) {
            // Estilos
            var stylesProperty = $.extend({}, defaultProperties.style);
            $.each(defaultProperties.style, function(key, value) {
                var style = selected.css(key);
                if (style !== '' && style !== 'undefined') {
                    stylesProperty[key] = style;
                }
            });
            // Classes
            var classProperty = selected.attr('class');
            // transition
            var transition = selected.data('transition');
            // bakground
            var background = selected.data('background');
            return {
                'style' : stylesProperty,
                'class' : classProperty,
                'data-transition' : transition,
                'data-background' : background
            };
        },

        /**
         * Seta a propriedade ao elemeto passado por parâmetro
         *
         * @param selected = elemento que receberá a propriedade
         * @param property = propriedade que será alterada separada por "." de acordo com a árvore default
         * @param value = valor da propriedade.
         */
        setProperty : function(selected, property, value) {
            slide[property](selected, value);
        },

        'data.data-background': function(element, value) {
            element.attr('data-background', value);
            element.css('background-color', value); // forja o estilo
        },

        'style.background-image': function(element, value) {
            element.css('background-image', 'url("'+value+'")');
        },

        'data.data-transition': function(element, value) {
            element.attr('data-transition', value);
        },

        getCommon: function() {
            var section = $('<section>', defaultProperties);
            section.append(element.getTitle());
            section.append(element.getText());
            return section;
        },

        getPoll: function(question) {
            var number = parseInt($('#hidden_poll_slide_increment').val()) + 1; //incrementa
            question = question || 'Sua pergunta?';
            var section = $('<section>', {
                'class':'poll',
                'data-number': number + '-' + $('#hidden_id').val(),
                'style': defaultProperties.style,
                'data-transition': defaultProperties['data-transition'],
                'data-background': defaultProperties['data-background']
            });
            section.append(element.getTitle(question));
            section.append(element.getOption('Opção 1', number));
            section.append(element.getOption('Opção 2', number));
            section.append(element.getOption('Opção 3', number));
            $('#hidden_poll_slide_increment').val(number); // atualiza o form
            return section;
        },

        getChat: function() {
            var section = $('<section>', {
                'class':'questions-slide',
                'style': defaultProperties.style,
                'data-transition': defaultProperties['data-transition'],
                'data-background': defaultProperties['data-background']
            });
            section.append(element.getTitle('Perguntas?'));
            return section;
        }
    };
}());

var element = (function () {
    // private
    var defaultProperties = {
        'style': {
            'font-family'     : "'Roboto Condensed', sans-serif",
            'font-size'       : '0.875em',
            'font-weight'     : '400', // bold
            'font-style'      : 'normal', // italic
            'text-decoration' : 'none', // underline
            'color'           : '#333333',
            'text-align'      : 'center', // left / center / right / justify
            'padding-left'    : '0'
        },
        'class': ''
    };

    // public
    return {
        getProperties : function(selected) {
            // Estilos
            var stylesProperty = $.extend({}, defaultProperties.style);
            $.each(defaultProperties.style, function(key, value) {
                var style = getStyleAttribute(selected, key);
                if (style !== '' && style !== 'undefined') {
                    stylesProperty[key] = style;
                }
            });
            // Classes
            var classProperty = selected.attr('class');
            return {
                'style' : stylesProperty,
                'class' : classProperty
            };
        },

        /**
         * Seta a propriedade ao elemeto passado por parâmetro
         *
         * @param selected = elemento que receberá a propriedade
         * @param property = propriedade que será alterada separada por "." de acordo com a árvore default
         * @param value = valor da propriedade.
         */
        setProperty: function(selected, property, value) {
            element[property](selected, value);
        },

        'class.fragment': function(element){
            if (element.hasClass('fragment')) {
                element.removeClass('fragment');
            } else {
                element.addClass('fragment');
            }
        },

        'style.font-family' : function(element, value){
            element.css('font-family', value);
        },

        'style.font-size' : function(element, value){
            element.css('font-size', value);
        },

        'style.font-weight' : function(element){
            var fontWeight = (element.css('font-weight') == 'bold') ? 'normal' : 'bold';
            element.css('font-weight', fontWeight);
        },

        'style.font-style' : function(element){
            var fontStyle = (element.css('font-style') == 'italic') ? 'normal' : 'italic';
            element.css('font-style', fontStyle);
        },

        'style.text-decoration' : function(element){
            var textDecoration = (element.css('text-decoration') == 'underline') ? 'none' : 'underline';
            element.css('text-decoration', textDecoration);
        },

        'style.color' : function(element, value){
            element.css('color', value);
        },

        'style.text-align' : function(element, value){
            element.css('text-align', value);
        },

        'style.padding-left' : function(element, value) {
            var initialPadding = getStyleAttribute(element, 'padding-left').replace('em','');
            var padding;
            if (value == 'increase'){
                padding = parseFloat(initialPadding) + parseFloat(1.5);
            } else {
                padding = parseFloat(initialPadding) - parseFloat(1.5);
            }
            padding = (padding < 1.5 ) ? 0 : padding;
            padding = (padding > 12.0) ? parseFloat(12.0) : padding;
            element.css('padding-left', padding + 'em');
        },

        /**
         * Insere título
         * @param importance = número de 1 a 6 que representa a importância do título
         */
        getTitle : function(text){
            var content = text || 'Título';
            var cloneStyle = $.extend({}, defaultProperties.style);
            var style = $.extend(cloneStyle, {
                'font-size'       : '1.5em',// 36px
                'font-weight'     : 'bold'
            });
            var element = $('<p>', {'css': style}).html(content);
            return element;
        },

        /**
         * Insere texto
         */
        getText: function(text) {
            var content = text || 'Texto';
            var element = $('<p>', {'css': defaultProperties.style}).html(content);
            return element;
        },

        /**
         * Insere lista não ordenada
         */
        getListUnordered : function() {
            var defaultStyle = $.extend({}, defaultProperties.style);
            var style = $.extend(defaultStyle, {
                'text-align'      : 'left',
                'padding-left'    : '1.5em'
            });
            var element = $('<ul>', {'css': style}).html('<li>Item de lista</li>');
            return element;
        },

        /**
         * Insere lista ordenada
         */
        getListOrdered: function() {
            var defaultStyle = $.extend({}, defaultProperties.style);
            var style = $.extend(defaultStyle, {
                'text-align'      : 'left',
                'padding-left'    : '1.5em'
            });
            var element = $('<ol>', {'css': style}).html('<li>Item de lista</li>');
            return element;
        },

        /**
         * Insere tabela
         * @param columns = Número de colunas
         * @param rows = Número de linhas
         */
        getTable: function(numRows, numCols){
            var element = $('<table>', {'class':'table-presentation'});
            for(i = 0 ; i < numRows ; i++) {
                var row = $('<tr>');
                for(j = 0 ; j < numCols ; j++) {
                    row.append($('<td>').html('texto'));
                }
                element.append(row);
            }
            return element;
        },

        /**
         * Insere imagem
         * @param url
         */
        getImage: function(url){
            var element = $('<div>').html(
                $('<img>', {'src': url,'alt': ''})
            );
            return element;
        },

        /**
         * Obtem um botão de resposta para o slide de pergunta
         *
         * @param text = texto da resposta apresentado no botão
         * @param idPoll = id do slide para montar um id único para o botão
         * @returns = elemento html
         */
        getOption: function(text, pollSlideIncrement) {
            var content = text || 'Resposta'; // caso não exista o texto
            var number = parseInt($('#hidden_button_poll_increment').val()) + 1;
            var dataValue = pollSlideIncrement + '-' + number;
            var element = $('<div>', {'class': 'button-level','data-value': dataValue}).html(
                '<span class="btn-content">'+content+'</span>' +
                '<div class="level green"></div>'
            );
            $('#hidden_button_poll_increment').val(number); // atualiza o form
            return element;
        }
    };
}());

/** ################## PRESENTATION ##################
 *  Manipula propriedades e funções da apresentação
 *  ##################################################
 */
var presentation = (function () {
    // public
    return {
        /**
         * Propriedades do elemento selecionado.
         */
        selectedElementProperties : {},

        /**
         * Seta o elemento como selecionado e obtem suas propriedades.
         * @param selected
         */
        setSelectedElement : function(selected){
            presentation.clearElementSelection();
            selected.addClass('active');
            presentation.selectedElementProperties = element.getProperties(selected);
            controls.refresh.all();
            presentation.applySlideChanges();
            presentation.save();
        },

        /**
         * Obtem o elemento selecionado no plano de edição.
         * @returns {HTMLElement}
         */
        getSelectedElement: function(){
            return $('.editable.active :nth-child(2)');
        },

        /**
         * Limpa a seleção no plano de edição.
         */
        clearElementSelection: function(){
            $("#slide-edit .editable").removeClass('active');
        },

        /**
         * Seta uma propriedade ao elemento selecionado.
         *
         * @param property = chave da propiedade
         * @param value = valor
         */
        setPropertyOnSelectedElement: function(property, value){
            var selected = presentation.getSelectedElement();
            element.setProperty(selected, property, value);
            presentation.applySlideChanges();
            presentation.save();
        },

        /**
         *
         * @param string slides
         */
        setSlides: function(slides){
            $('#slide-list').html(slides);
        },

        getSlides: function(){
            return $('#slide-list').html();
        },

        /**
         * Quantidade de slides da apresentação.
         */
        countSlides: function() {
            var count = $('#slide-list section').length;
            return count;
        },

        /**
         * Seta uma propriedade ao slide selecionado.
         *
         * @param property = chave da propiedade
         * @param value = valor
         */
        setPropertyOnSelectedSlide: function(property, value){
            var selected = presentation.getSelectedSlide();
            slide.setProperty(selected, property, value);
            presentation.applySlideChanges();
            presentation.save();
        },

        getSelectedSlide: function() {
            return $("#slide-edit section");
        },

        getSelectedSlideType: function() {
            var slide = presentation.getSelectedSlide();
            if (slide.hasClass('common')){
                return 'common';
            } else if (slide.hasClass('poll')) {
                return 'poll';
            } else if (slide.hasClass('chat')) {
                return 'chat';
            }
        },

        setSelectedSlide: function(selected) {
            $("#slide-list section").removeClass('active');
            selected.addClass('active');
            $("#slide-edit").html(''); // limpa a edição
            var slide = selected.clone(); // clona o slide
            slide.removeAttr('draggable');
            slide.html(''); // limpa o conteúdo do clone
            $('h1, h2, h3, h4, h5, h6, p, ul, ol, img, table, div.button-level', selected).each(function() {
                var editable = $('<div>', {'class':'editable'});
                var cloneElement = $(this).clone();
                // trata o conteúdo do elemento que será editável
                if (cloneElement.hasClass('button-level')) {
                    cloneElement.find('span.btn-content').attr('contenteditable', 'true').find('span.val').remove();
                } else {
                    cloneElement.attr('contenteditable', 'true');
                }
                editable.html(cloneElement);
                // insere os controles
                var controls = $('<div>', {'class': 'selected-controls'}).html(
                    $('<a>', {'href':'javascript:void(0)','class': 'bt-selected-close'}).html('<i class="fa fa-times"></i>')
                );
                editable.prepend(controls);
                slide.append(editable);
            });
            $("#slide-edit").html(slide); // adciona ao painel de edição
            presentation.refreshSortable();
            controls.refresh.all();
        },

        /**
         * Trnasporta as alterações do slide editado para a lista
         * deve ser invokado sempre que se deseje atualizar a lista
         * com as últimas alterações e antes de salvar.
         */
        applySlideChanges: function() {
            var editable = $('#slide-edit section');
            var slide = editable.clone(); // clona o slide editado
            var selectedSlide = $('#slide-list section.active');
            selectedSlide.html(''); // limpa o conteúdo do slide da lista
            selectedSlide.attr('class', slide.attr('class') + ' active');
            selectedSlide.attr('style', slide.attr('style'));
            selectedSlide.attr('data-transition', slide.attr('data-transition'));
            selectedSlide.attr('data-background', slide.attr('data-background'));
            $('h1, h2, h3, h4, h5, h6, p, ul, ol, img, table, div.button-level', slide).each(function() {
                var cloneElement = $(this).clone();
                cloneElement.removeAttr('contenteditable');
                if (cloneElement.hasClass('button-level')) { // se for botão de resposta
                    cloneElement.find('span.btn-content').removeAttr('contenteditable').append('<span class="val">&nbsp;(<b>0</b>)</span>');
                    if ($('#slide-list section.active').find('.buttons').length) { // se ja tem a div .buttons
                        $('#slide-list section.active .buttons').append(cloneElement);
                    } else { // se não cria antes de inserir
                        var buttons = $('<div>', {'class':'buttons fragment'});
                        buttons.append(cloneElement);
                        $('#slide-list section.active').append(buttons);
                    }
                } else {
                    $('#slide-list section.active').append(cloneElement);
                }
            });
        },

        /**
         * Remove o slide selecionado.
         */
        removeSelectedSlide : function() {
            var selected = $("#slide-list section.active");
            if(selected.length === 0){
                alert('Nenhum slide selecionado!');
            } else if(confirm('Deseja realmente remover o slide selecionado?')){
                var parent = selected.prev();
                if(parent.length === 0){
                    parent = selected.next();
                }
                selected.remove();
                if(parent.length > 0) {
                    presentation.setSelectedSlide(parent);
                } else {
                    $('#slide-edit').html('');
                }
                presentation.applySlideChanges();
                presentation.save();
            }
        },

        insertSlide : function(slide){
            $('#slide-list').append(slide);
            $('#slide-list').sortable({});
            this.setSelectedSlide($('#slide-list').find('section:last-of-type'));
            presentation.applySlideChanges();
            presentation.save();
        },

        /**
         * Insere elemento previamente formatado
         * @param element
         */
        insertElement : function(element) {
            var editable = $('<div>', {'class':'editable active'});
            element.attr('contenteditable', 'true');
            editable.html(element);
            // insere os controles
            var controls = $('<div>', {'class': 'selected-controls'}).html(
                $('<a>', {'href':'javascript:void(0)','class': 'bt-selected-close'}).html('<i class="fa fa-times"></i>')
            );
            editable.prepend(controls);
            // obtem o elemento selecionado caso exista
            var $selectedElement = $('#slide-edit').find('.editable.active');
            presentation.clearElementSelection();
            if ($selectedElement.length > 0) {
                $selectedElement.after(editable);
            } else {
                $('#slide-edit section').append(editable);
            }
            presentation.refreshSortable();
            presentation.applySlideChanges();
            presentation.save();
        },

        refreshSortable: function() {
            // restarta o plugin
            $('#slide-edit section').sortable({}).bind('sortstop', function() {
                presentation.applySlideChanges();
                presentation.save();
            });
        },

        /**
         * Carrega os itens da apresentação a partir dos dados
         * carregados no formulário.
         * Invocado no carregamento da página.
         */
        loadPresentationForm: function() {
            // name
            $('#txt-titulo').val($('#hidden_name').val());
            // download PDF
            var checked = ($('#hidden_download_pdf').val() == 1);
            $('#bt-download-pdf').prop('checked', checked);
            // html
            presentation.setSlides($('#hidden_html').val());
            var first = $('#slide-list section:first-child');
            if (first.length) {
                presentation.setSelectedSlide(first);
            }
        },

        /**
         * Atualiza o form com os dados da apresentação
         * Deve ser invocado antes de persistir.
         */
        sendDataToForm: function() {
            $('#hidden_name').val($('#txt-titulo').val());
            $('#hidden_html').val(presentation.getSlides());
            // hidden_forbidden_words são preenchidos no memento da edição
            // bt-download-pdf é preenchido no momento da edição
        },

        save: function() {
            presentation.applySlideChanges();
            presentation.sendDataToForm();

            $.ajax({
                url: '/admin/presentation/save/' + $('#hidden_id').val(),
                data: $('form#presentation').serialize(),
                type: 'POST',
                beforeSend: function() {
                    $('.loading').css('display', 'inline-block');
                },
                success: function(data) {
                    $('.loading').fadeOut(2000, function(){
                        $(this).hide();
                    });
                },
                error: function(e){
                    alert('Erro ao tentar salvar apresentação');
                }
            });
        },

        validateStart: function() {
            var local = $('#presentation-local').val();
            if (local === '' || local.length < 3) {
                modal.printMessage('Informe o local onde será realizada a apresentação!<br />Deve conter no mínimo 3 caracteres.', 'error');
                return false;
            }
            return true;
        },

        start: function() {
            var user   = $('#presentation-user').val();
            var token  = $('#presentation-token').val();
            var local  = $('#presentation-local').val();
            var amount = $('#presentation-amount-people').val();
            $.ajax({
                url: '/presentation/start',
                data: {
                    user: user,
                    token: token,
                    local: local,
                    amountPeople: amount
                },
                type: 'POST',
                beforeSend: function() {
                    modal.disable();
                    modal.clearMessage();
                    modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp; Processando...', 'success');

                    setTimeout(function(){
                        modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp; Aguarde, Gerando PDF...', 'success');
                    }, 3000);
                },
                success: function(result) {
                    if (result.status && result.status === 'success') {
                        modal.printMessage('Iniciando a apresentação! <i class="fa fa-spinner fa-pulse"></i>&nbsp;Redirecionando...', 'success');
                        window.location.href = '/presentation';
                    } else if (result.status && result.status === 'error' ) {
                        modal.enable();
                        modal.printMessage(result.message, 'error');
                    } else {
                        modal.enable();
                        modal.printMessage('Sem resposta da operação!', 'error');
                    }
                },
                error: function(e){
                    modal.enable();
                    modal.printMessage('Sem resposta da operação!', 'error');
                }
            });
        }
    };
}());

/** ###################### CONTROLS ##########################
 * Métodos e atributos para manipular os controles de interface
 * ###########################################################
 */
var controls = (function () {
    // private
    var properties = {
        slide: {
            style: {
                /*'background-color' : function(value){
                    if (typeof value !== 'undefined') {
                        $('#bt-background-color').val(rgb2hex(value));
                    }
                }*/
            },
            data: {
                'data-transition': function(value) {
                    $('#sl-transition').val(value);
                },
                'data-background': function(value) {
                    $('.bg-color-picker .add-on i').css('background-color', value);
                }
            }
        },
        element : {
            style : {
                'font-family' : function(value) {
                    $('#sl-font-family').val(value);
                },
                'font-size' : function(value) {
                    $('#sl-font-size').val(value);
                },
                'font-weight' : function(value) {
                    $('#bt-font-weight').addClass('active');
                    if(value == 'bold') {
                        $('#bt-font-weight').addClass('active');
                    } else {
                        $('#bt-font-weight').removeClass('active');
                    }
                },
                'font-style' : function(value) {
                    if(value == 'normal') {
                        $('#bt-font-style').removeClass('active');
                    } else {
                        $('#bt-font-style').addClass('active');
                    }
                },
                'text-decoration' : function(value) {
                    if (value == 'none') {
                        $('#bt-text-decoration').removeClass('active');
                    } else {
                        $('#bt-text-decoration').addClass('active');
                    }
                },
                'color' : function(value) {
                    $('.color-picker .add-on i').css('background-color', value);
                },
                'text-align' : function(value) {
                    $('#bt-text-align-left').removeClass('active');
                    $('#bt-text-align-center').removeClass('active');
                    $('#bt-text-align-right').removeClass('active');
                    $('#bt-text-align-justify').removeClass('active');
                    if (value == 'start' || value == 'left'){
                        $('#bt-text-align-left').addClass('active');
                    } else if(value == 'center') {
                        $('#bt-text-align-center').addClass('active');
                    } else if(value == 'right') {
                        $('#bt-text-align-right').addClass('active');
                    } else if(value == 'justify') {
                        $('#bt-text-align-justify').addClass('active');
                    }
                },
                'padding-left' : function(value) {

                }
            },
            'class'  : function(value){
                if (value == 'fragment') {
                    $('#bt-fragment').addClass('active');
                } else {
                    $('#bt-fragment').removeClass('active');
                }
            }
        }
    };
    // public
    return {
        refresh : {
            all : function() {
                var selectedElement = presentation.getSelectedElement();
                var selectedSlide = presentation.getSelectedSlide();
                var selectedElementProperties = element.getProperties(selectedElement);
                var selectedSlideProperties = slide.getProperties(selectedSlide);
                $('.control').each(function(){
                    var target = $(this).data('target');
                    var currentProperty = $(this).data('property'); // ex: "style.font-family"
                    var arr = currentProperty.split('.'); // ex: "style.font-weight"
                    var propertyKey = arr[0];
                    var propertyField = arr[1];
                    if (typeof propertyKey != 'undefined' && typeof propertyField != 'undefined') {
                        // slide
                        if (target === 'slide') {
                            //console.log(propertyKey);
                            //console.log(propertyField);
                            if (propertyKey === 'data') {
                                //console.log(selectedSlideProperties[propertyField]);
                                properties.slide[propertyKey][propertyField](selectedSlideProperties[propertyField]);
                            } else {
                                properties.slide[propertyKey][propertyField](selectedSlideProperties[propertyKey][propertyField]);
                            }
                        // element
                        } else {
                            if (propertyKey === 'class') {
                                properties.element[propertyKey](selectedElementProperties[propertyKey]);
                            } else {
                                properties.element[propertyKey][propertyField](selectedElementProperties[propertyKey][propertyField]);
                            }
                        }
                    }
                });
            }
        }
    };
}());

var modal = (function () {
    // public
    return {
        clearMessage: function() {
            $('.modal .modal-body .alert').remove();
        },
        printMessage: function(message, type) {
            modal.clearMessage();
            $('.modal .modal-body').prepend('<div class="alert alert-'+ type +'">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message + '</div>');
        },
        disable: function() {
            $('.modal button, input').prop("disabled",true);
        },
        enable: function() {
            $('.modal button, input').prop("disabled",false);
        }
    }
}());

/** ##################### EVENTS ###########################
 * Associação de eventos jQuery
 * #########################################################
 */
$(document).ready( function() {

    // ativa o color picker(style.color)
    $('.color-picker').colorpicker({
        format: 'hex',
        colorSelectors: [
            '#000000', '#333333','#666666','#999999','#cccccc','#ffffff','#ff0000','#00ff00','#034d86'
        ]
    }).on('changeColor.colorpicker', function(event){
        //bodyStyle.backgroundColor = event.color.toHex();
        var property = 'style.color';
        var value = event.color.toHex();
        presentation.setPropertyOnSelectedElement(property, value);
        controls.refresh.all();
    });

    // ativa o color picker(style.background-color)
    $('.bg-color-picker').colorpicker({
        format: 'hex',
        colorSelectors: [
            '#000000', '#333333','#666666','#999999','#cccccc','#ffffff','#ff0000','#00ff00','#034d86'
        ]
    }).on('changeColor.colorpicker', function(event){
        //bodyStyle.backgroundColor = event.color.toHex();
        var property = 'data.data-background';
        var value = event.color.toHex();
        presentation.setPropertyOnSelectedSlide(property, value);
        controls.refresh.all();
    });

    // carrega os dados
    presentation.loadPresentationForm();

    // active tabs do formulário
    $('#tabs').tab();

    // sortable lista
    $('#slide-list').sortable({}).bind('sortstop', function() {
        presentation.applySlideChanges();
        presentation.save();
    });

    // sortable item
    $('#slide-edit section').sortable({}).bind('sortstop', function() {
        presentation.applySlideChanges();
        presentation.save();
    });

    // cria o primeiro slide caso não exista nenhum
    if (presentation.countSlides() <= 0) {
        presentation.insertSlide(slide.getCommon());
    }

    // EVENTS ---------------------------------------------------
    $('#fileupload1').bind('fileuploaddestroy', function(e, data){
        //console.log(data);
        // TODO: remover imagem da apresentaçao e de fundo caso em uso
    });

    $("#slide-list").delegate('section','click', function() {
        presentation.applySlideChanges();
        presentation.setSelectedSlide($(this));
    });

    // ao selecionar um item editável
    $("#slide-edit").delegate( ".editable", "click", function() {
        presentation.setSelectedElement($(this));
    }).delegate( ".bt-selected-close", "click", function() {
        // remove o elemento
        $(this).parents().closest('.editable').remove();
        presentation.applySlideChanges();
        presentation.save();
    });

    $("#bt-slide-delete").on('click', function() {
        presentation.removeSelectedSlide();
    });

    $("#bt-insert-title").on('click', function() {
        presentation.insertElement(element.getTitle());
    });

    $("#bt-insert-paragraph").on('click', function() {
        presentation.insertElement(element.getText());
    });

    $("#bt-insert-question").on('click', function() {
        var slideType = presentation.getSelectedSlideType();
        if (slideType !== 'poll') {
            alert('Questões só podem ser inseridas em slides do tipo questão!');
            return false;
        }
        var number = presentation.getSelectedSlide().data('number');
        presentation.insertElement(element.getOption('Resposta', number));
    });

    $("#bt-insert-ul").on('click', function() {
        presentation.insertElement(element.getListUnordered());
    });

    $("#bt-insert-ol").on('click', function() {
        presentation.insertElement(element.getListOrdered());
    });

    $("#bt-insert-table").on('click', function() {
        // TODO: validar quantidade de row e col
        var row = $('#input-table-row').val();
        var col = $('#input-table-col').val();
        presentation.insertElement(element.getTable(row, col));
    });

    $("#bt-background-image-slide").on('click', function() {
        insertImage = 'SLIDE';
        $('#bt-remove-image').show();
    });

    $("#bt-insert-image").on('click', function() {
        insertImage = 'ELEMENT';
        $('#bt-remove-image').hide();
    });

    // remove imagem de fundo do slide
    $("#bt-remove-image").on('click', function() {
        presentation.setPropertyOnSelectedSlide('style.background-image', 'none');
    });

    $("#insert-image").delegate(".select", "click", function() {
        var parent = $(this).parents('tr');
        var img = parent.find('td.preview img');
        var srcThumbnail = img.attr('src');
        srcThumbnail = srcThumbnail.replace(/http?:\/\/[^\/]+/i, "");
        var srcImg = srcThumbnail.replace("thumbnail/", "");
        if (insertImage == 'ELEMENT') {
            presentation.insertElement(element.getImage(srcImg));
        } else {
            presentation.setPropertyOnSelectedSlide('style.background-image', srcImg);
        }
    });

    $("#bt-moderator").on('click', function() {
        $('#forbidden-words').val($('#hidden_forbidden_words').val());
    });

    $("#bt-save-moderator").on('click', function() {
        $('#hidden_forbidden_words').val($('#forbidden-words').val());
    });

    // change select .control
    $("select.control").on('change', function() {
        var property = $(this).data('property');
        var value = $(this).val();
        var target = $(this).data('target');
        if(target === 'slide') {
            presentation.setPropertyOnSelectedSlide(property, value);
        } else {
            presentation.setPropertyOnSelectedElement(property, value);
        }

    });

    // click button .control
    $("button.control").on('click', function() {
        var target = $(this).data('target');
        var property = $(this).data('property');
        var value = $(this).data('value');
        if(typeof target === 'undefined'){
            presentation.setPropertyOnSelectedElement(property, value);
        }
        controls.refresh.all();
    });

    // inserir slide comum
    $("#bt-slide-common").on('click', function() {
        presentation.insertSlide(slide.getCommon());
    });

    // inserir slide de pergunta
    $("#bt-slide-poll").on('click', function() {
        presentation.insertSlide(slide.getPoll());
    });

    // inserir slide chat
    $("#bt-slide-chat").on('click', function() {
        presentation.insertSlide(slide.getChat());
    });

    // dowload PDF
    $("#bt-download-pdf").on('click', function() {
        var value = ($(this).is(':checked')) ? 1 : 0;
        $('#hidden_download_pdf').val(value);
        presentation.save();
    });

    $("#bt-save").on('click', function() {
        presentation.applySlideChanges();
        presentation.save();
    });

    $("#bt-preview, #bt-play").on('click', function() {
        presentation.applySlideChanges();
        presentation.save();
    });

    // salvamento automático a cada 30 segundos apenas por garantia
    setInterval(function(){
        presentation.applySlideChanges();
        presentation.save();
    }, 30000);

    $("#presentation-amount-people").mask('9?9999');
    $("a.start-presentation").on('click', function() {
        if($(this).hasClass('btn-runnable')) {
            window.location.href = '/presentation';
            return false;
        }
        modal.clearMessage();
        modal.enable();
        $('#presentation-local').val('').focus();
        $('#presentation-token').val($(this).data('token'));
    });

    $("#bt-start").on('click', function() {
        if (presentation.validateStart() && presentation.start()) {
            $('#modal-start').modal('hide');
        }
    });

});// document.read

// AUX FUNCTIONS --------------------------------------
function rgb2hex(rgb) {
    if(typeof rgb !== 'undefined'){
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? "#" +
        ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
    } else {
        return '#ffffff';
    }
}

/**
 * Obtem o valor do estilo CSS incluido no atributo style da tag
 * e não o valor computado pelo browser.
 *
 * @param element = elemento no qual se deseja buscar o valor.
 * @param searchProp = propriedade procurada.
 * @returns {string} valor da propriedade css buscada do atributo style.
 */
function getStyleAttribute(element, searchProp) {
    var result = '';
    var attrStyle = element.attr('style');
    if (attrStyle) {
        var arrStyles = attrStyle.split(';');
        $.each(arrStyles, function(key, style) {
            var arr = style.trim().split(':');
            if (typeof arr[0] !== 'undefined' && typeof arr[1] !== 'undefined') {
                var property = arr[0];
                var value = arr[1];
                if (property.trim() == searchProp) {
                    result = value.trim();
                    return;
                }
            }
        });
    }
    return result;
}
