$(document).ready(function() {

    var formChanged = false;

    $('form :input').change(function(){
        formChanged = true;
    });

    window.onbeforeunload = function (evt) {
        if(formChanged){
            var message = 'As alterações ainda não foram salvas!';
            if (typeof evt == 'undefined') {
                evt = window.event;
            }
            if (evt) {
                evt.returnValue = message;
            }
            return message;
        }
    };

    $('#save').click(function() {
        formChanged = false;
        $('form[name="alterar_senha"]').submit();
    });

    //bootstrap tolltips
    $('.tool_tip').tooltip({
        placement: 'right'
    });

    $('form[name="alterar_senha"]').submit(function() {

    }).validate({
        ignore: '',
        // Define as regras
        rules:{
            'usuario[senhaAtual]': {
                required: true,
                minlength: 4
            },
            'usuario[senha]': {
                required: true,
                minlength: 4
            },
            'usuario[confirmacao]':{
                required: true,
                equalTo: "#senha"
            }
        },
        highlight: function(element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        errorPlacement: function(label, element) {
            // posiciona o label de erro
            if (element.is("input[type=radio]")){
                label.insertAfter($(element).closest('div.controls'));
            } else {
                label.insertAfter(element);
            }
        },
        success: function(element) {
            element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        },
        invalidHandler: function(form, validator) {
            $('form#alterar_senha div.alert').remove();
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
                printAlert("Atenção, preencha corretamente todos os campos obrigatórios.", "error");

            }
        }
    });

    function printAlert(message, type) {
        $('form#alterar_senha').prepend('<div class="alert alert-'+ type +'">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            message + '</div>');
    }
});