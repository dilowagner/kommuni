var oTable;
var giRedraw = false;

$(document).ready(function() {

    // click na linha do grid
    $(".datagrid tbody").click(function(event) {

        if($(event.target.parentNode).hasClass('selected')){
            $(event.target.parentNode).removeClass('selected');
        }else{
            $(event.target.parentNode).addClass('selected');
        }

    });

    // duplo tap na linha do grid (especifico para tablets e afins)
    $(".datagrid tbody").doubletap(function() {
        dataTableDblClick();
    });

    // duplo click na linha do grid
    $(".datagrid tbody").dblclick(function() {
        dataTableDblClick();
    });

    // DATAGRID
    oTable = $(".datagrid").dataTable({
        "bStateSave": false,
        "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Todos"]],
        "sPaginationType": "full_numbers",
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs":
        [
            { "bSearchable": true, "bVisible": true, "aTargets": [ 0 ] },
            { "bSearchable": true, "bVisible": true, "aTargets": [ 1 ] },
            { "bSearchable": false, "bVisible": true, "aTargets": [ 2 ] }
        ],
        "language": {
            "url": "../plugins/data-table-1.10.5/add-on/Portuguese-Brasil.json"
        }
    }).columnFilter({
        sPlaceHolder: "head:after",
        aoColumns:
        [
            { type: "text" },
            { type: "text" },
            { type: "text" },
            { type: "select", values: [ "Sim", "Não"] },
            { type: "text" }
        ]
    });

    // EDIT
    $('a#edit').click(function() {
        edit();
    });

    // SELECT ALL
    $("a#select_all").click(function() {
     $(oTable.fnSettings().aoData).each(function (){
     $(this.nTr).addClass('selected');
     });
     });

    // SELECT NONE
    $("a#select_none").click(function() {
     $(oTable.fnSettings().aoData).each(function (){
     $(this.nTr).removeClass('selected');
     });
     });


    // VIEW
    $('a#view').click(function() {
     view();
     });

    // DELETE
    $('a#delete').click(function() {

     var ids = "";
     var count = 0;

     // percorre as linhas selecionadas
     oTable.$("tr").filter(".selected").each(function (index, row){
     var nTds = $('td', row);
     var id = $(nTds[0]).text();

     ids+="-" + id;
     count++;
     });

     if(count>=1) {
     ids = ids.substring(1, ids.length);
     idsMsg = ids.split("-");
     idsMsg = idsMsg.join(", ");
     idsMsg = idsMsg.replace(/,\s([^,]+)$/, ' e $1');

     var msg = "";
     if(count>1)
     msg = "Deseja realmente excluir os registros <b>" + idsMsg + "</b> ?";
     else
     msg = "Deseja realmente excluir o registro <b>" + idsMsg + "</b> ?";

     $.msgBox({
     title: "Excluir?",
     content: msg,
     type: "confirm",
     buttons: [{ value: "Sim" }, { value: "Não" }, { value: "Cancelar"}],
     success: function (result) {
     if (result == "Sim") {
     remove(ids);
     }
     }
     });

     }else{

     $.msgBox({
     title:"Atenção",
     content:"Nenhum registro selecionado!",
     type:"info"
     });

     }

     });

});// document.read

function edit() {

    var anSelected = fnGetSelected( oTable );

    var nTds = $('td', anSelected);
    var id = $(nTds[0]).text();

    if (id){
        var host = window.location.toString();
        $(window.location).attr('href', host + '/editar/' + id);
        ev.stopPropagation();
    } else {

        $.msgBox({
            title:"Atenção",
            content:"Nenhum registro selecionado!",
            type:"info"
        });

    }

}


function view(id) {

 $('.datagrid tbody tr').each(function(ev) {

 if ($(this).hasClass('selected')){
 var nTds = $('td', this);
 var id = $(nTds[0]).text();
 var host = window.location.toString();
 $(window.location).attr('href', host + '/view/' + id);
 ev.stopPropagation();
 }

 });

 $.msgBox({
 title:"Atenção",
 content:"Nenhum registro selecionado!",
 type:"info"
 });

 }

function remove(ids) {
 var host = window.location.toString();
 $(window.location).attr('href', host + '/delete/' + ids);
 }

function fnGetSelected( oTableLocal ) {

    var aReturn = new Array();
    var aTrs = oTableLocal.fnGetNodes();

    for ( var i=0 ; i<aTrs.length ; i++ ) {

        if ( $(aTrs[i]).hasClass('selected') ) {
            aReturn.push( aTrs[i] );
        }
    }
    return aReturn;
}

function dataTableDblClick() {

    $(oTable.fnSettings().aoData).each(function (){
        $(this.nTr).removeClass('selected');
    });

    $(event.target.parentNode).addClass('selected');

    edit();

}