$(function(){
    $('.nickname .button').on('tap', function(event) {

        event.preventDefault();
        var nickname = $('.nickname input[type="text"]').val();

        if (nickname != '') {

            $('.nickname .ajax').show();
            $('.nickname .button').hide();

            $.post('/nickname', {nickname: nickname}, function(data) {

                $('.nickname .ajax').hide();
                $('.nickname .message').html(data.message);

                if (data.success) {
                    setTimeout(function() {
                        $('.nickname').hide();
                        window.location.href = '/presentation';
                    }, 3000);
                } else {
                    $('.nickname .button').show();
                }
            }, 'json');
        } else {
            $('.nickname .message').html("Digite seu apelido.");
        }
    });
});