$(document).ready( function() {

	var formChanged = false;
	
	$('form :input').change(function(){
		   formChanged = true;
	});
	
	window.onbeforeunload = function (evt) {
		if(formChanged){
			var message = 'As alterações ainda não foram salvas!';
			if (typeof evt == 'undefined') {
				evt = window.event;
			}
			if (evt) {
				evt.returnValue = message;
			}
		  return message;
		}
	};

    //bootstrap tolltips
    $('.tool_tip').tooltip({
        placement: 'bottom'
    });
	
	$('#save').click(function() {  
		formChanged = false;
		$('form[name="cadastro-usuario"]').submit();
	});    
	
	 $('form[name="cadastro-usuario"]').submit(function() {
		
	 }).validate({ 	
			ignore: '',
     	// Define as regras
        rules:{
            'usuario[nome]':{
                required: true,
                minlength: 4
            },
            'usuario[usuario]':{
	            required: true,
                minlength: 4
	        },
            'usuario[senha]': {
                required: true,
                minlength: 4
            },
            'usuario[confirmacao]':{
                required: true,
                equalTo: "#senha"
            },
            'usuario[email]': {
                required: true,
                email: true
            },
            'usuario[regra]': {
                required: true
            }
        },
        // Define as mensagens de erro para cada regra
        messages:{

        },
         highlight: function(element) {
             $(element).closest('.control-group').removeClass('success').addClass('error');
         },
         success: function(element) {
             element
                 .text('OK!').addClass('valid')
                 .closest('.control-group').removeClass('error').addClass('success');
         },
         invalidHandler: function(form, validator) {

             var errors = validator.numberOfInvalids();
             if (errors) {
                 validator.errorList[0].element.focus();
                 alert("Atenção, preencha corretamente todos os campos obrigatórios.");

             }
         }
        
    });
    
});// document.read