var Modules = {};

Modules.presentation = {

	start: function () {

		Reveal.addEventListener('slidechanged', function( event ) {

			if(Reveal.isLastSlide()) {
				ratchet.end();
				if(TYPE=='PRESENTER') {
					Reveal.configure({controls: false, keyboard: false, history: true});
					Modules.presentation.finish();
				}
			}

			var isPoll      = $(event.previousSlide).hasClass('poll');
			var isQuestions = $(event.previousSlide).hasClass('questions-slide');

			if ((isPoll || isQuestions) && TYPE=='PRESENTER') {

				var reportId = $('.report-id').val();
				var slide    = '';
				var identity = '';

				if(isPoll) {
					slide      = $(Reveal.getPreviousSlide()).html();
					var number = $(event.previousSlide).data('number');
					identity   = reportId + '-' + number;
				} else {
					slide    = $('.messages').html();
					identity = reportId + '-response';
				}
				var type = isPoll ? 'E' : 'Q';

				var registry = window.sessionStorage.getItem('report-' + identity);
				if(null == registry) {
					registry = 0;
				}

				if(! (registry > 0 && type == 'Q')) {
					Modules.presentation.saveReport(reportId, registry, slide, type, identity);
				}
			}
		});
	},

	saveReport: function(report, registry, slide, type, identity) {
		$.ajax({
			url: '/report',
			data: {
				id: report,
				registry: registry,
				slide: slide,
				type: type
			},
			type: 'POST',
			beforeSend: function() {
			},
			success: function(data) {
				window.sessionStorage.setItem('report-' + identity, data.registry);
			},
			error: function(e){
				alert('Erro ao tentar salvar relatorio');
			}
		});
	},

	finish: function() {

		window.sessionStorage.clear();
		var token = $('input.token').val();

		$.ajax({
			url: '/presentation/finish',
			data: {token: token},
			type: 'POST',
			beforeSend: function() {
			},
			success: function(data) {
				ratchet.emit('close', {flag: true});
				$('.btn-back-presentation').show();
			},
			error: function(e){
				alert('Erro ao tentar finalizar a apresentação.');
			}
		});
	}
};

Modules.questions = {
	start: function () {
		Reveal.addEventListener('slidechanged', function( event ) {

			var questionsSlide = $(event.currentSlide).hasClass('questions-slide');
			if (questionsSlide) {
			    if (! Modules.questions.initialized) {
					Modules.questions.init();
				}
			} else {
				if (Modules.questions.initialized) {
					Modules.questions.initialized = false;
					$('div.questions').hide();
				}
			}
		});

		$('div.questions').on('click', '.button', function(event) {
			event.preventDefault();

			var inputDoubt = $('input.doubt');
			var question   = inputDoubt.val();

			var inputForbiddenWords = $('input.forbidden-words');
			var forbiddenWords      = inputForbiddenWords.val();

			if (question != '') {
				ratchet.emit('question', {question: question, forbiddenWords: forbiddenWords});
				inputDoubt.val('');
			}
		});

		ratchet.on(1, this.callback);
	},

	callback: function(data) {
		if (data.type == 'question') {
			var messages = $('div.questions .messages');
			if (messages.length > 0) {
				messages.prepend('<div class="message"><p><strong>' + data.nickname + '</strong>: ' + data.question + '</p></div>');
			}
		}
	},

	initialized: false,

	init: function() {
		var section   = $('.questions-slide');
		var questions = $('div.questions');
		questions.show();
		if(TYPE=='SPECTATOR') {

			section.html('');
			questions.html('<p>' +
				'<input type="text" placeholder="Envie sua pergunta..." name="doubt" class="doubt" /></p>' +
				'<div class="button">' +
					'<span>ENVIAR</span>' +
				'</div>' +
				'<div class="ajax" style="display: none;">' +
					'<i class="fa fa-spinner fa-pulse" style="font-size: 30px;"></i>' +
				'</div>'
			);
			section.html(questions);
		} else {
			questions.html(
				'<div class="messages">' +
					'<div class="content"></div>' +
				'</div>'
			);
		}
		Modules.questions.initialized = true;
	}
};

Modules.poll = {
	start: function() {
		//Listener dos botões
		$('.poll .button-level').on('tap', function(event) {

            event.preventDefault();
			var poll = $(this).closest('.poll');
			if (! $(poll).hasClass('votado')) {

				var number = $(poll).attr('data-number');
				var vote   = $(this).attr('data-value');

				var slide  = number;
				if(! Modules.poll.hasVoted(slide)) {

					var voted = Modules.poll.attachNewVoted(slide);
					window.sessionStorage.setItem('poll-storage', voted);

					ratchet.emit('poll', {number: number, value: vote});
					$(poll).addClass('votado');
				}
			}
		});

		$('.poll .button-level span').on('tap', function(event) {
			event.preventDefault();
		});
		//Listener do socket
		ratchet.on(1, this.callback);
	},

	attachNewVoted: function(slide) {
		var votes = window.sessionStorage.getItem('poll-storage');
		if(null == votes) {
			return slide;
		}
		return votes + ";" + slide;
	},

	hasVoted: function(slide) {

		var votes = window.sessionStorage.getItem('poll-storage');
		if(null == votes) {
			return false;
		}
		var slideVoted = votes.split(';');
		return slideVoted[0].search(slide) != -1;
	},

	callback: function(data) {
		if (data.type == 'poll-result') {
			var poll = $('.poll[data-number="' + data.number + '"]');

			$.each(data.votes, function(index, quantity) {
				poll.find('.button-level[data-value="' + index + '"]').find('span b').html(quantity);
			});

			$.each(data.percentages, function(index, quantity) {
				poll.find('.button-level[data-value="' + index + '"]').find('.level').height(quantity + '%');
			});
		}
	}
};

//Inicia todos os módulos
for(var propertyName in Modules) {
   Modules[propertyName].start();
}