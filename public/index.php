<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

$protocol = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
$alias    = isset($_SERVER['REDIRECT_BASE']) ?  substr_replace($_SERVER['REDIRECT_BASE'], '', -1) : null;

if(array_key_exists('argc', $_SERVER) && $_SERVER['argc'] > 0){
    define('BASE_URL', 'console' );
}else{
    define('PROTOCOL', sprintf('%s://', $protocol));
    define('HOST', $_SERVER['HTTP_HOST']);
    define('BASE_URL', sprintf('%s%s%s', PROTOCOL, HOST, $alias));
}

define('BASE_PATH', realpath(__DIR__));

// Setup autoloading
require 'init_autoloader.php';
ini_set('display_errors', 1);
// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
