Para mudar o diretorio dos arquivos é necessário dar um rebuild no plugin utilizando o codigo fonte, para isso
altere o src do arquivo src/rotator/tools/ModuleLoader.js e ececute o arquivo build.bat
por fim utilize os aqruivos da pasta build.


Step 1: Arquivos necessários:

At first you need to put required files beside your page: Go to
 "source/compressed" and copy all JavaScript files to "js/cute"
 directory beside your page. Go to "source/assets" and copy "cute-theme"
 folder beside your page. Go to "source/assets/style" and copy "slider-style.css"
 to "style" directory beside your page. Then add following html to the <head> of your page.

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.1/modernizr.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/cute/cute.slider.js" type="text/javascript" charset="utf- 8"></script>
<script src="js/cute/cute.transitions.all.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="style/slider-style.css" type="text/css" media="screen"  charset="utf-8"/>
<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js" type="text/javascript" charset="utf-8">
</script>

esses arquivos já estao disponiveis e compilados com a estrutura de pastas atual...


Step 2: Estilo CSS básico:

<style>
#my-cute-slider{
    position:relative;
}

#slider-wrapper{
    position:relative;
    min-width:300px;
    max-width:800px;
    margin: 0 auto;
    }
</style>

Step 3: HTML básico

Add following html to the <body> of your page: In this code determines that the slide images
 are located in "images" and the thumbnail images are in "images/thumbs" beside your page.

<div id="slider-wrapper">
    <div id="my-cute-slider" data-width="800" data-height="350">
        <ul data-type="slides">
            <li data-delay="4" data-trans3d="tr28" data-trans2d="tr1">
                <img src="images/1.jpg" data-thumb="images/thumbs/1.jpg"/>
            </li>
            <li data-delay="4" data-trans3d="tr29" data-trans2d="tr1">
                <img src="theme/blank.jpg"  data-src="images/3.jpg" data-thumb="images/thumbs/3.jpg"/>
            </li>
        </ul>     <ul data-type="controls">
            <li data-type="circletimer"> </li>
            <li data-type="next"> </li>
            <li data-type="previous"> </li>
            <li data-type="slidecontrol"> </li>
            <li data-type="bartimer"> </li>
        </ul>
        <div class="br-shadow">
        </div>
    </div>
</div>


Step 4: Js que inicia o plugin:

Add following js at the end of <body> in your page:

<script>
    var myslider = new Cute.Slider();
    myslider.setup("my-cute-slider" , "slider-wrapper");
</script>

pode ser utilizado tambem no document.ready