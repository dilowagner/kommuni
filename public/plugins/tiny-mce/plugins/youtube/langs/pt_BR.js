tinymce.addI18n('en',{
	'Choose YouTube Video'  : 'Procurar Video do YouTube',
	'Insert Youtube video'  : 'Inserir Video do YouTube',
	'width'					: 'Largura',
	'height'				: 'Altura',
	'skin'					: 'Tema',
	'dark'          		: 'dark',
	'light'         		: 'light',
	'Search'        		: 'Procurar',
	'Youtube URL'   		: 'Youtube URL',
	'Title'         		: 'Titulo',
	'Insert and Close'		: 'Inserir e fechar',
	'Insert'				: 'Inserir',
	'Load More'				: 'Carregar mais',
	'cancel'				: 'cancelar'
});
