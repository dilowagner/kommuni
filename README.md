Palestra Interativa - v1.0
=======================

Introdução
------------
Projeto consiste em criar um sistema para elaborador apresentações de slides interativos, onde o espectador/aluno
poderá interagir em tempo real com o palestrante/professor.
Projeto será desenvolvido pelos formandos do curso de Ciência da Computação da IES/FASC.

#### Requirements
------------
* PHP 5.4+
* [Zend Framework 2](https://github.com/zendframework/zf2)