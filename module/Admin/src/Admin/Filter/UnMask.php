<?php
/**
 * Class UnMask
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Base\Filter;

use Zend\Filter\Exception;
use Zend\Filter\FilterInterface;

class UnMask implements FilterInterface
{
    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException
     * @return mixed
     */
    public function filter($value)
    {
        return preg_replace('/[^\d]/', null, $value);
    }
}