<?php
/**
 * Class DateLocal
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Base\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class DateLocal extends AbstractFilter
{
    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        //implode('-',array_reverse(explode('/',$value)))
        return \DateTime::createFromFormat('d/m/Y', $value);
    }

}