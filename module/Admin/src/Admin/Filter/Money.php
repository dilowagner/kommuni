<?php
/**
 * Class Money
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Base\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class Money extends AbstractFilter
{
    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        $money = str_replace(',', '.', str_replace(['R$', '.', ' '], null, $value));
        return $money;
    }

}