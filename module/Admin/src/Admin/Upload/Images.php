<?php
namespace Base\Upload;

class Images {

	private $path = '/uploads/files/';
	private $imgPath;
	private $thumbnailPath;
	private $entity;
	private $id;
	private $extensionsFilter;

	public function __construct($entity, $id, $extensionsFilter = array('.png','.gif','.jpg','.jpeg','.PNG','.GIF','.JPG','.JPEG')){
		$this->entity = $entity;
		$this->id = $id;
		$this->extensionsFilter = $extensionsFilter;
		$this->imgPath = $this->path . $entity . '/' . $id . '/';
		$this->thumbnailPath = $this->path . $entity . '/' . $id . '/thumbnail/';
	}

	public function getImgPath() {
		return $this->imgPath;
	}

	public function getThumbnailPath() {
		return $this->thumbnailPath;
	}

	public function findAll() {
		$path = __DIR__.'/../../../../../public'. $this->path . $this->entity .'/' . $this->id;
		if(! is_dir($path))
			return -1;

		// Abrindo diretório
		$d = dir($path);
		$i = 0;
		$array = array();

		if($this->extensionsFilter == 'dir'){
			while (false !== ($entry = $d->read())) {
				if(substr_count($entry, '.') == 0){
					$array[$i] = $entry;
					$i++;
				}
			}
		}else{
			while (false !== ($entry = $d->read())) {
				if (is_array( $this->extensionsFilter ) ) {
					foreach ($this->extensionsFilter as $row) {
						if(substr_count($entry, $row) == 1){
							$array[$i] = $entry;
							$i++;
						}
					}
				} else {
					if(substr_count($entry, $this->extensionsFilter) == 1){
						$array[$i] = $entry;
						$i++;
					}
				}
			}

		}
		//Fechando diretorio
		$d->close();

		if(! empty($array)){
			// Colocando em ordem alfabetica
			sort ($array);
			// Voltando o ponteiro para o inicio da matriz
			reset ($array);
			// Retornado resultado final
		}

		return $array;
	}



	public function findFirst() {
		$images = $this->findAll();

		if($images) {
			return $images[0];
		}else{
			return false;
		}
	}

	public function findLast() {


	}

	public function count() {
		$images = $this->findAll();
		return count($images);
	}


	/**
	 * @method deleteDirectories()
	 * Este método limpa as imagens nas pastas
	 * onde serão colocados as fotos das noticias, empresa e agenda, etc..
	 *
	 * É usada principalmente nos métodos onNew das classes Noticia, Agenda e Empresa
	 *
	 * @param string $dir
	 * @static
	 *
	 * @author Diego Wagner
	 */
	public function deleteDirectories( $dir ){

		if( is_dir($dir) ) {
			// abre o diretório
			$dh = opendir ( $dir );
			// faz a leitura do mesmo
			while ( $file = readdir ( $dh ) ) {
				if ( $file != "." && $file != ".." ) {
					$fullpath = $dir . "/" . $file;
					// verifica se é arquivo ou diretório
					if ( ! is_dir ( $fullpath ) ) {
						unlink ( $fullpath );
					} else {
						// deleta recursivamente
						$this->deleteDirectories( $fullpath );
					}
				}
			}
			// fecha o diretório
			closedir ( $dh );
		}
	}

}
