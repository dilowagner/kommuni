<?php
namespace Admin\Controller;

use Admin\Enum\FlashMessages;
use Admin\Exception\BusinessException;
use Admin\Exception\InvalidInstanceException;
use Log\Interfaces\LoggerAwareInterface;
use Log\Traits\LoggerAwareTrait;
use Admin\Interfaces\ObjectEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Form;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;


abstract class AbstractCrudController extends AbstractActionController implements LoggerAwareInterface
{

    use LoggerAwareTrait;

	protected $service;
	protected $entity;
	protected $form;


    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     * @access public
     */
    public function indexAction() {

        try {
            $list = $this->service->findAll();
            $paginator = new Paginator(new ArrayAdapter($list));
            $paginator->setItemCountPerPage(20);
            return new ViewModel(['data' => $paginator]);

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }

    }

    /**
     * @method newAction()
     * Criar um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function newAction()
    {
        try {

            $request = $this->getRequest();
            if ( $request->isPost() ) {
                $this->form->setData($request->getPost());
                if ( $this->form->isValid() ) {
                    if ( $this->service->save( $this->form->getData() ) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                        $this->redirectToIndex();
                    }
                }
            }
            return $this->renderView(['form' => $this->form]);

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
            return $this->redirectToIndex();
        }
    }

	/**
	 * @method viewAction()
	 * Visualiza um registro
	 * @return \Zend\View\Model\ViewModel
	 * @access public
	 */
	public function viewAction()
    {
		try {
			
			$id = $this->params()->fromRoute('id',0);
			$form = new $this->form();
			$this->disableFormFields( $form );
		
			if($id) {
				$entity = $this->service->getReference($this->entity, $id);
                if (!($entity instanceof ObjectEntity))
                    throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);

                $form->bind($entity);
			    return $this->renderView(['form' => $form]);
			}
		
		} catch( \Exception $e ) {
			$this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
		    return $this->redirectToIndex();
		}
	}

    /**
     * @override
     * @method editAction()
     * Editar um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        try {
            $id = $this->params()->fromRoute('id', 0);
            $entity = $this->service->find( $id );
            if (!($entity instanceof ObjectEntity))
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);

            $this->form->bind($entity);
            $request = $this->getRequest();

            if ( $request->isPost() ) {
                $this->form->setData($request->getPost());
                if ( $this->form->isValid() ) {
                    if ( $this->service->save( $this->form->getData() ) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                        return $this->redirectToIndex();
                    }
                }
            }
            return $this->renderView(['form' => $this->form,'id' => $id]);

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

	/**
	 * @method deleteAction()
	 * Deleta o registro
	 * @return \Zend\View\Model\ViewModel
	 * @access public
	 */
	public function deleteAction()
    {
		try {
			$params = $this->params()->fromRoute('id');
			if ( isset ( $params ) ) 
			    $ids = explode('-', $params);
			
			if ( is_array( $ids ) ) {
				$successMessages = array();
				$errorMessages = array();
				foreach ( $ids as $id ) {
					$entity = $this->service->find($id);
                    if (!($entity instanceof ObjectEntity))
                        throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);

					$result = $this->service->remove( $entity );
					if ( $result == $id ) {
						$successMessages[] = 'Registro ' . $result . ' excluído com sucesso!';
					} else {
						$errorMessages[] = 'Erro ao tentar excluir o registro ' . $id .'!';
					}
				}

				$this->addMessages( array('success' => $successMessages,'error'   => $errorMessages));
				return $this->redirectToIndex();
				
			} else  
			     throw new \Exception("Parâmetros não encontrados");

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
            return $this->redirectToIndex();
        }
	}

	/**
	 * Seta o script.phtml que está em outro lugar.
	 * @return ViewModel
	 * @access protected
	 */
	protected function renderView( Array $vars, $disableLayout = null )
    {
		$view = new ViewModel();
	
		$rotulos = array('new' =>'Novo Registro',
				         'edit'=>'Editar Registro');

        //$controller = explode(DIRECTORY_SEPARATOR, $this->getCurrentControllerName());
        $controller = explode('\\', $this->getCurrentControllerName());
        $controllerName = end($controller);
        // explode a string por camelCase
        preg_match_all('/((?:^|[A-Z])[a-z]+)/', $controllerName, $matches);
        // junta as palavras com "-" e casa baixa
        $controllerName = strtolower(implode('-', $matches[0]));

        $folderView = strtolower($this->getCurrentNamespace());
		$actionName = $this->getEvent()->getRouteMatch()->getParam('action', 'NA');
		$view->setTemplate( $folderView . '/' . $controllerName . '/crud' );

		if($disableLayout) // desabilita o layout
			$view->setTerminal( true );
	
		$vars['action']	 = $actionName;
        if (isset($rotulos[$actionName]))
		    $vars['rotuloAction']	= $rotulos[$actionName];
	
		return $view->setVariables($vars);
	
	}

    /**
     * Retorna as mensagens do flashMessenger
     *
     * @method getMessages()
     * @return array
     */
    public function getMessages()
    {
        return $this->flashMessenger()->setNamespace($this->getCurrentNamespace())->getMessages();
    }

    /**
     * Adiciona as mensagens no flashMessenger
     *
     * @method addMessages( array $messages )
     * @param array $messages
     * @return $this
     */
    public function addMessages( array $messages )
    {
        $this->flashMessenger()->setNamespace($this->getCurrentNamespace())->addMessage( $messages );
        return $this;
    }

    /**
     * Retorna o namespace corrente
     * EX: Control\Controller\EmpresaController
     * retorna: Control
     *
     * @method getCurrentNamespace()
     * @return string
     */
    public function getCurrentNamespace()
    {
        //$controllerClass = explode(DIRECTORY_SEPARATOR, get_class($this));
        $controllerClass = explode('\\', get_class($this));
        return reset($controllerClass);
    }

    /**
     * Retorna o noome do controller corrente
     *
     * @method getCurrentControllerName()
     * @return string
     */
    public function getCurrentControllerName()
    {
        return $this->params('controller');
    }

    /**
     * Retorna o nome da rota corrente
     *
     * @method getCurrentRouteName()
     * @return string
     */
    public function getCurrentRouteName()
    {
        $router = $this->getServiceLocator()->get('router');
        $request = $this->getServiceLocator()->get('request');
        $routeMatch = $router->match($request);
        return $routeMatch->getMatchedRouteName();
    }

	/**
	 * Seta os campos do formulário como desabilitado
	 * @param Form $form
	 * @access protected
	 */
	protected function disableFormFields( Form $form )
    {
		$elements = $form->getElements();
		foreach ( $elements as $element ) {
			$element->setAttributes(['readonly' => true, 'disabled' => true]);
		}
	}

    /**
     * Redireciona o usuário novamente para a listagem
     * @method redirectToIndex()
     * @access protected
     * @return \Zend\Http\Response
     */
    protected function redirectToIndex()
    {
        return $this->redirect()->toRoute(
            $this->getCurrentRouteName(),
            array(
                'controller' => strtolower($this->getCurrentControllerName())
            )
        );
    }

    /**
     * Redireciona o usuário novamente para a listagem
     * @method redirectToIndex()
     * @access protected
     * @return \Zend\Http\Response
     */
    protected function redirectToHome()
    {
        return $this->redirect()->toRoute(
            'admin-index',
             array(
                 'controller' => 'index'
             )
        );
    }
}
