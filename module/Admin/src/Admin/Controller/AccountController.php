<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Acl for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin\Controller;

use Admin\Adapter\Adapter;
use Admin\Adapter\AuthAdapter;
use Admin\Entity\User;
use Admin\Interfaces\ObjectEntity;
use Admin\Interfaces\PublicAccessInterface;
use Admin\Service\UserService;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

class AccountController extends AbstractCrudController implements PublicAccessInterface
{
    /**
     * @param FormInterface $form
     * @param UserService $service
     */
    public function __construct(FormInterface $form, UserService $service)
    {
        $this->form    = $form;
        $this->service = $service;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function indexAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $form    = $this->form;
        $user    = new User();

        if($request->isPost()) {
            $post = $request->getPost();
            $form->bind($user);
            $form->setData($post);
            if($form->isValid()) {

                /** @var ObjectEntity $user */
                $user = $form->getData();
                $result = $this->service->save($user);
                if($result) {
                    // sessão que grava a autenticação
                    $sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
                    $auth = new AuthenticationService();
                    $auth->setStorage($sessionStorage);
                    $sessionStorage->write($user);

                    return $this->redirect()->toRoute('admin-index');
                }
            }
        }
        return new ViewModel(['form' => $this->form]);
    }
}
