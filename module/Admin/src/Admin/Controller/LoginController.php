<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Acl for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin\Controller;

use Admin\Adapter\Adapter;
use Admin\Adapter\AuthAdapter;
use Admin\Interfaces\LoginAwareInterface;
use App\Service\PresentationService;
use Zend\Http\Request;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

use Admin\Form\Login as LoginForm;

class LoginController extends AbstractCrudController implements LoginAwareInterface
{
    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function loginAction()
    {
        $form = new LoginForm();
        /** @var Request $request */
        $request = $this->getRequest();

        if($request->isPost()) {
            $post = $request->getPost();
            $form->setData($post);
            if($form->isValid()) {

                $serviceLocator = $this->getServiceLocator();
                // sessão que grava a autenticação
                $sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
                $auth = new AuthenticationService();
                $auth->setStorage($sessionStorage);

                /** @var Adapter $authAdapter */
                $authAdapter = $serviceLocator->get('admin.auth.adapter');
                $authAdapter->setMail($post->mail);
                $authAdapter->setPassword($post->password);

                $result = $auth->authenticate($authAdapter);

                if($result->isValid()) {
                    $user = $auth->getIdentity();
                    // escreve os dados na sessão
                    $sessionStorage->write($user);
                    /** @var PresentationService $presentationService */
                    $presentationService = $serviceLocator->get('app.service.presentation');
                    // reseta todas as apresentações do usuário
                    $presentationService->updatePresentationToNotRunnableByUser($user);

                    return $this->redirect()->toRoute('admin-index');
                } else {
                    $this->flashMessenger()->addWarningMessage($result->getMessages()[0]);
                }
            }
        }
        return new ViewModel(['form' => $form]);
    }

    /**
     * @method logoutAction
     * Responsavel por gerenciar logout's de usuarios
     */
    public function logoutAction()
    {
    	$auth = new AuthenticationService();
    	$auth->setStorage(new SessionStorage(AuthAdapter::USER_SESSION));
    	$auth->clearIdentity();

    	return $this->redirect()->toRoute('admin-login');
    }
}
