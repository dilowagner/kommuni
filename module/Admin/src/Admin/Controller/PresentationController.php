<?php
namespace Admin\Controller;

use Admin\Entity\Presentation;
use Admin\Entity\User;
use Admin\Interfaces\UserProviderInterface;
use Admin\Service\PresentationService;
use Admin\Form\Presentation as PresentationForm;
use Admin\Util\Token;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Admin\Enum\FlashMessages;
use Admin\Exception\BusinessException;
use Admin\Exception\InvalidInstanceException;

class PresentationController extends AbstractCrudController implements UserProviderInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param PresentationService $presentationService
     */
    public function __construct(PresentationService $presentationService)
    {
        $this->service = $presentationService;
    }

    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     * @access public
     */
    public function indexAction() {

        try {
            $user = $this->getUser();
            $list = $this->service->findBy(['user' => $user], ['id' => 'desc']);
            $paginator = new Paginator(new ArrayAdapter($list));
            return new ViewModel(['data' => $paginator]);

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function editAction()
    {
        try{
            $id = $this->params()->fromRoute('id', 0);
            $user = $this->getUser();
            $presentation = $this->service->findOneBy(['id' => $id, 'user' => $user]);
            if (!($presentation instanceof Presentation)) {
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);
            }

            $form = new PresentationForm();
            $form->bind($presentation);

            return new ViewModel(
                [
                    'form'         => $form,
                    'presentation' => $presentation
                ]
            );

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

    /**
     * Persiste as informações no banco de dados
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            $arrayReturn = array();
            // Se for uma chamada ajax
            if ($request->isXmlHttpRequest()) {

                $id   = $this->params()->fromPost('id', null);
                $name = $this->params()->fromPost('name');
                $html = $this->params()->fromPost('html', null);
                $forbiddenWords = $this->params()->fromPost('forbiddenWords', null);
                $downloadPdf = $this->params()->fromPost('downloadPdf', 0);

                if($id){
                    $entity = $this->service->find($id);
                } else {
                    $entity = new Presentation();
                    $entity->setToken(new Token());
                    $user = $this->getUser();
                    $entity->setUser($user);
                }

                $entity->setName($name);
                $entity->setHtml($html);
                $entity->setForbiddenWords($forbiddenWords);
                $entity->setDownloadPdf($downloadPdf);
                // save
                $result = $this->service->savePresentation($entity);

                if($result instanceof Presentation) {
                    $arrayReturn['status'] = 'success';
                    $arrayReturn['id'] = $result->getId();
                } else {
                    $arrayReturn['status'] = 'error';
                    $arrayReturn['message'] = 'Erro ao tentar criar apresentação!';
                }
            }
        } catch (\Exception $e) {
            $arrayReturn['status'] = 'error';
            $arrayReturn['message'] = $e->getMessage();
        }
        $jsonModel = new JsonModel($arrayReturn);
        $jsonModel->setTerminal(true);

        return $jsonModel;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function copyAction()
    {
        try{
            $id   = $this->params()->fromRoute('id', 0);
            $user = $this->getUser();
            $presentation = $this->service->findOneBy(['id' => $id, 'user' => $user]);
            if (!($presentation instanceof Presentation)) {
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);
            }

            $this->service->copyPresentation($presentation);
            $this->flashMessenger()->addSuccessMessage("Apresentação copiada com sucesso.");

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function stopAction()
    {
        try{
            $id   = $this->params()->fromRoute('id', 0);
            $user = $this->getUser();
            $presentation = $this->service->findOneBy(['id' => $id, 'user' => $user]);
            if (!($presentation instanceof Presentation)) {
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);
            }

            $this->service->stopPresentation($presentation);
            $this->flashMessenger()->addSuccessMessage("Apresentação foi interrompida com sucesso.");

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

    /**
     * @method deleteAction()
     * Deleta o registro
     * @return \Zend\View\Model\ViewModel
     * @access public
     */
    public function deleteAction()
    {
        try{
            $id   = $this->params()->fromRoute('id', 0);
            $user = $this->getUser();
            $presentation = $this->service->findOneBy(['id' => $id, 'user' => $user]);
            if (! ($presentation instanceof Presentation)) {
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);
            }

            $this->service->remove($presentation);
            $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_REMOVER);

        } catch (BusinessException $b) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch(\Exception $e) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}