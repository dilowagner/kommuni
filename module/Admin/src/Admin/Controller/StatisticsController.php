<?php
namespace Admin\Controller;

use Admin\Entity\Presentation;
use Admin\Entity\User;
use Admin\Interfaces\UserProviderInterface;
use Admin\Service\PresentationService;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use Admin\Enum\FlashMessages;
use Admin\Exception\BusinessException;
use Admin\Exception\InvalidInstanceException;
use Admin\Interfaces\ObjectEntity;

class StatisticsController extends AbstractCrudController implements UserProviderInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param PresentationService $presentationService
     */
    public function __construct(PresentationService $presentationService)
    {
        $this->service = $presentationService;
    }

    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     * @access public
     */
    public function indexAction() {

        try {
            $user = $this->getUser();
            $list = $this->service->findBy(['user' => $user], ['id' => 'desc']);
            $paginator = new Paginator(new ArrayAdapter($list));
            return new ViewModel(['data' => $paginator]);

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function viewAction()
    {
        try{
            $id   = $this->params()->fromRoute('id', 0);
            $user = $this->getUser();
            $presentation = $this->service->findOneBy(['id' => $id, 'user' => $user]);
            if (! ($presentation instanceof Presentation)) {
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);
            }

            return new ViewModel(['presentation' => $presentation]);

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}