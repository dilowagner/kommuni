<?php
namespace Admin\Controller;

use Admin\Entity\User;
use Admin\Enum\FlashMessages;
use Admin\Interfaces\UserProviderInterface;
use Admin\Service\UserService;
use Admin\Exception\BusinessException;
use Admin\Exception\InvalidInstanceException;
use Admin\Interfaces\ObjectEntity;
use Zend\Form\FormInterface;
use Zend\View\Model\ViewModel;

class UserController extends AbstractCrudController implements UserProviderInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var FormInterface
     */
    private $formChangePassword;

    public function __construct(FormInterface $form, FormInterface $formChangePassword, UserService $service)
    {
        $this->form               = $form;
        $this->formChangePassword = $formChangePassword;
        $this->service 	          = $service;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function editionAction()
    {
        $form = $this->form;
        $user = $this->getUser();

        try {

            $form->bind($user);
            $form->setValidationGroup(array(
                'user' => array(
                    'name',
                    'mail',
                    'active'
                )
            ));
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setData($request->getPost());
                if($form->isValid()) {
                    $user = $form->getData();
                    if ( $this->service->save($user) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                    }
                }
            }
            return new ViewModel(['form' => $form]);

        } catch (InvalidInstanceException $i) {
            $this->flashMessenger()->addInfoMessage($i->getMessage());
            $this->getLogger()->info($i->getMessage());
        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToHome();
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function changePasswordAction()
    {
        $user = $this->getUser();
        try {

            $formChangePassword = $this->formChangePassword;
            $formChangePassword->bind($user);
            $request = $this->getRequest();

            if ( $request->isPost() ) {

                $formChangePassword->setData($request->getPost());

                if($formChangePassword->isValid()) {
                    $user = $formChangePassword->getData();
                    if ( $this->service->save($user) ) {
                        $this->flashMessenger()
                             ->addSuccessMessage(
                                 FlashMessages::SUCESSO_PADRAO_SALVAR . " Sua senha foi alterada, deseja sair do sistema? <a href='/admin/logout'>Clique aqui</a>"
                             );
                    }
                }
            }
            return new ViewModel(['form' => $formChangePassword]);

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToHome();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
