<?php
namespace Admin\Controller;

use Zend\View\Model\ViewModel;

class IndexController extends AbstractCrudController
{
    public function indexAction()
    {
    	return new ViewModel();
    }
}
