<?php
namespace Admin\Controller\Factory;

use Admin\Controller\UserController;
use Admin\Form\User;
use Admin\Form\ChangePassword;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get(User::class);
        $formChangePassword = $formManager->get(ChangePassword::class);
        $service = $serviceLocatorInstance->get('admin.service.user');

        return new UserController($form, $formChangePassword, $service);
    }
}
