<?php
namespace Admin\Controller\Factory;

use Admin\Controller\AccountController;
use Admin\Form\User;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AccountControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get(User::class);
        $service = $serviceLocatorInstance->get('admin.service.user');

        return new AccountController($form, $service);
    }
}
