<?php
namespace Admin\Controller\Factory;

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 14/03/14
 * Time: 22:29
 */

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractControllerFactory implements AbstractFactoryInterface {

    /**
     * Determinar se podemos criar o servico com o nome
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        if ( class_exists( $requestedName . 'Controller' ))
            return true;
        else
            return false;
    }

    /**
     * Cria o servico com o nome
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return mixed
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        $formManager   = $serviceLocatorInstance->get('FormElementManager');

        $controller = $requestedName . 'Controller';

        return new $controller( $formManager );
    }
}