<?php
namespace Admin\Controller\Factory;

use Admin\Controller\PresentationController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Admin\Adapter\AuthAdapter;


class PresentationControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return PresentationController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        // Service
        $service = $serviceLocatorInstance->get('Admin\Service\Presentation');

        return new PresentationController($service);
    }
}
