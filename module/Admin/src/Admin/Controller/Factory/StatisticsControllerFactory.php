<?php
namespace Admin\Controller\Factory;

use Admin\Controller\StatisticsController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class StatisticsControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return PresentationController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        // Service
        $service = $serviceLocatorInstance->get('Admin\Service\Presentation');

        return new StatisticsController($service);
    }
}
