<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Service;

use Admin\Adapter\Adapter;
use Admin\Entity\User;

/**
 * Class UsuarioService
 * @package Admin\Service
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class UserService extends AbstractService
{
    /**
     * Retorna o objeto referência do usuário Logado
     *
     * @method getUsuarioLogado()
     * @return mixed
     */
    public function getUsuarioLogado()
    {
        $adapter = new Adapter();
        return $this->getReference(User::class, $adapter->getUserId());
    }
} 