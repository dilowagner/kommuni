<?php
namespace Admin\Service\Factory;

use Admin\Entity\Presentation;
use Admin\Service\PresentationService;
use Zend\ServiceManager\FactoryInterface;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;

class PresentationServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $objectManager    = $serviceLocator->get(EntityManager::class);
        $objectRepository = $objectManager->getRepository(Presentation::class);

        return new PresentationService($objectManager, $objectRepository);
    }
}