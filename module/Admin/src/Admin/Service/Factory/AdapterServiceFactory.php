<?php
/**
 * Class Factory
 * @author Diego Wagner
 */
namespace Admin\Service\Factory;

use Admin\Adapter\Adapter;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AdapterServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return Adapter
     */
	public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get(EntityManager::class);
		return new Adapter($entityManager);
	}
}