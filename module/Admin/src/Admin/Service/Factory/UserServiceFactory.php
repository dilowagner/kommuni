<?php
namespace Admin\Service\Factory;

use Admin\Entity\User;
use Admin\Service\UserService;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $objectManager    = $serviceLocator->get(EntityManager::class);
        $objectRepository = $objectManager->getRepository(User::class);

        return new UserService($objectManager, $objectRepository);
    }
}