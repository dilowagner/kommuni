<?php
namespace Admin\Service;

use Admin\Exception\NotImplementedException;
use Admin\Util\Files;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Admin\Interfaces\ObjectEntity;
use Doctrine\ORM\EntityManager;

/**
 * @class AbstractService
 * 
 * @package Admin\Service
 * @author Diego
 *
 */
abstract class AbstractService
{
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    /**
     * @param ObjectManager $objectManager
     * @param ObjectRepository $objectRepository
     */
	public function __construct(ObjectManager $objectManager, ObjectRepository $objectRepository)
    {
		$this->objectManager = $objectManager;
        $this->repository    = $objectRepository;
	}

    /**
     * Retorna as entidades a serem iteradas
     *
     * @method findAll()
     * @return array Object
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Retorna um object pelos criterios passados por parametro
     *
     * @method findOneBy( array $criteria )
     * @param array $criteria
     * @return object
     */
    public function findOneBy( array $criteria )
    {
        return $this->repository->findOneBy( $criteria );
    }

    /**
     * Retorna apenas uma entidade pelo seu ID
     *
     * @method find($id)
     * @param $id
     * @return object
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Retorna a referencia do objeto passado por parâmetro
     *
     * @method getReference()
     * @param string $entityClass
     * @param int $id
     * @return mixed
     */
    public function getReference($entityClass, $id)
    {
        return $this->objectManager->getReference($entityClass, $id);
    }

    /**
     * @param ObjectEntity $entity
     * @return ObjectEntity
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
	public function save(ObjectEntity $entity)
    {
        /** @var EntityManager $objectManager */
        $objectManager = $this->objectManager;
        $connection    = $objectManager->getConnection();
        // BEGIN TRANSACTION
        $connection->beginTransaction();
        try {

            if(null == $entity->getId()) {
                $objectManager->persist($entity);
            }
            $objectManager->flush();
            // COMMIT
            $connection->commit();
            // renomeia a pasta de imagens
            //$this->renameImageFolder($entity);
            return $entity;

        } catch ( \Exception $e ) {
            // ROLLBACK
            $connection->rollback();
            $objectManager->close();
            throw new \Exception("ERRO: Ao salvar os dados no banco \r" . $e->getMessage());
        }
	}

    /**
     * @param ObjectEntity $entity
     * @return mixed
     * @throws \Exception
     */
	public function remove(ObjectEntity $entity)
    {
        /** @var EntityManager $objectManager */
        $objectManager = $this->objectManager;
        $connection    = $objectManager->getConnection();
        // BEGIN TRANSACTION
        $connection->beginTransaction();

        try {

            $id = $entity->getId();
            // remove
            $objectManager->remove($entity);
            $objectManager->flush();
            // COMMIT
            $connection->commit();

//            // remove as imagens
//            $dir = $this->imagesPath . $id;
//
//            if( is_dir($dir) ){
//                $files = new Files();
//                $files->removeDirectory($dir);
//            }

            return $id;

        } catch ( \Exception $e ) {
            // ROLLBACK
            $connection->rollback();
            $objectManager->close();
            throw new \Exception("ERRO: Ao remover os dados no banco \r" . $e->getMessage());
        }
	}

	/**
	 * renameImageFolder
     *
	 * @param $entity
	 */
	protected function renameImageFolder( ObjectEntity $entity )
    {
        if ( method_exists ($entity , 'getImageFolders') ) {

            // renomeia a pasta de imagens para o caminho definitivo
            $adapter = new Adapter();

            $lastFolderCapa = 'new_' . $adapter->getUserId() . '_capa';
            $lastFolder = 'new_' . $adapter->getUserId();

            $folders = $entity->getImageFolders();

            foreach ($folders as $folder) {

                //var_dump($folder.$lastFolder);
                //var_dump(is_dir($folder.$lastFolder));die;
                // verifica se a pasta existe
                if(is_dir($folder.$lastFolderCapa)){
                    // renomeia a pasta de imagens
                    rename($folder.$lastFolderCapa, $folder.$entity->getId().'_capa');
                }

                // verifica se a pasta existe
                if(is_dir($folder.$lastFolder)){
                    // renomeia a pasta de imagens
                    rename($folder.$lastFolder, $folder.$entity->getId());
                }

            }

        } else {
            throw new NotImplementedException('Método getImageFolders não implementado na entidade');
        }
	}
}
