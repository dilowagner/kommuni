<?php
namespace Admin\Service;


use Admin\Entity\Presentation;
use Admin\Interfaces\ObjectEntity;
use Admin\Util\Token;

class PresentationService extends AbstractService
{
    /**
     * É necessário realizar o MERGE na entidade... pois a entidade User está serializada na Session
     * o que causa erro de persistência, devido a mesma ser um objeto Proxy
     * @param Presentation $presentation
     * @return \Admin\Interfaces\ObjectEntity
     * @throws \Exception
     */
    public function savePresentation(Presentation $presentation)
    {
        /** @var ObjectEntity $presentation */
        $presentation = $this->objectManager->merge($presentation);
        return parent::save($presentation);
    }

    /**
     * @param Presentation $presentation
     * @return ObjectEntity
     * @throws \Exception
     */
    public function copyPresentation(Presentation $presentation)
    {
        $copy = clone $presentation;
        $copy->setId(null);
        $copy->setName($copy->getName() . ' - Cópia');
        $copy->setToken(new Token());
        $copy->setRunnable(false);
        $copy->getReports()->clear();

        return parent::save($copy);
    }

    /**
     * @param Presentation $presentation
     * @return ObjectEntity
     * @throws \Exception
     */
    public function stopPresentation(Presentation $presentation)
    {
        $presentation->setRunnable(false);
        return parent::save($presentation);
    }
}