<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 19/03/14
 * Time: 20:45
 */

namespace Admin\Exception;


/**
 * Class BusinessException
 * Esta classe irá concentrar todas as exceptions de regras de negócio
 *
 * @package Base\Exception
 * @author DiegoWagner <diegowagner4@gmail.com>
 *
 * @see \Exception
 * @throws \Excetion
 */
class BusinessException extends \Exception {

} 