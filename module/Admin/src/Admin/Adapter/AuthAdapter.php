<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Adapter;

use Admin\Interfaces\LoginAwareInterface;
use Admin\Interfaces\PublicAccessInterface;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ServiceManager\ServiceManager;

class AuthAdapter
{
    
    const USER_SESSION  = 'USER_SESSION';
    /**
     * 
     * @var MvcEvent
     */
    private $mvcEvent;

    /**
     * @var ServiceManager
     */
    private $serviceManager;
    
    
    public function __construct( MvcEvent $e )
    {
        $this->mvcEvent       = $e;
        $this->serviceManager = $e->getApplication()->getServiceManager();
    }

    /**
     * Método por processar toda vez em que o evento DISPATCH
     * for executado pra ver se o usuário está logado ou não
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function processAuthentication()
    {
        $controller = $this->mvcEvent->getTarget();
        $controllerClass = get_class( $controller );
        $moduleName = strtolower( substr( $controllerClass, 0, strpos( $controllerClass, '\\' ) ) );
        if ((!$controller instanceof LoginAwareInterface) && (!$controller instanceof PublicAccessInterface)) {
            $this->processModules($moduleName);
        }
    }

    /**
     * Processa a autenticação e autorização dos módulos
     *
     * @method processModules($moduleName)
     * @param string $moduleName
     * @return \Zend\Stdlib\ResponseInterface
     * @access private
     * @throws \Exception
     */
    private function processModules($moduleName)
    {
        $config = $this->serviceManager->get('Config');
        if (! isset($config['acl']))
            throw new \Exception('Configurações da ACL não foram encontradas');

        $authentication = new AuthenticationService();
        $authentication->setStorage(new SessionStorage(self::USER_SESSION));

        if (in_array($moduleName, $config['acl']['modules'])) {
            if ( !$authentication->hasIdentity() ) {
                return $this->processResponse('admin-login');
            }
            return true;
        }
    }

    /**
     * @param $route
     * @return \Zend\Stdlib\ResponseInterface
     */
    private function processResponse($route)
    {
        $url = $this->mvcEvent->getRouter()->assemble(array(), array( 'name' => $route ) );
        /** @var Response $response */
        $response = $this->mvcEvent->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $url);
        $response->setStatusCode(302);
        $response->sendHeaders();
        
        return $response;
    }
}