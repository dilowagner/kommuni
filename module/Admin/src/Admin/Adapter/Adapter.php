<?php
namespace Admin\Adapter;

use Admin\Entity\User;
use Admin\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

class Adapter implements AdapterInterface
{
	/**
	 * 
	 * @var EntityManager $em
	 */
	protected $em;
	
	/**
	 * 
	 * @var string $mail
	 */
	protected $mail;
	
	/**
	 * 
	 * @var string $password;
	 */
	protected $password;
	
	/**
	 * @method __construct()
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em = null)
    {
		$this->em = $em;
	}

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @see \Zend\Authentication\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate()
    {
        try {
            /** @var UserRepository $repository */
            $repository = $this->em->getRepository(User::class);
            return $repository->findByMailAndPassword($this->getMail(), $this->getPassword());
        }catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
	 * @return int
	 */
	public function getUserId()
    {
		$sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
		$authService = new AuthenticationService();
		$authService->setStorage($sessionStorage);
		
		if($authService->hasIdentity())
			return $authService->getIdentity()->getId();
	}
}