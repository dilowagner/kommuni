<?php
namespace Admin\Repository;

use Admin\Entity\User;
use Doctrine\ORM\EntityRepository;
use Zend\Authentication\Result;

class UserRepository extends EntityRepository
{
    /**
     * @param $mail
     * @param $password
     * @return Result
     * @throws \Exception
     */
    public function findByMailAndPassword($mail, $password)
    {
        try {
            /** @var User $user */
            $user = $this->findOneBy(['mail' => $mail]);
            if (! is_null($user)) {
                $hashPassword = $user->encryptPassword($password);
                if (($user->getPassword() == $hashPassword) && $user->isActive()) {
                    return new Result(Result::SUCCESS, $user, array('Autenticado com sucesso'));
                }
            }
            return new Result(Result::FAILURE, null, array('Usuário ou senha inválidos.'));
        } catch(\Exception $e) {
            throw $e;
        }
    }
}