<?php
/**
 * Class PresentationRepository
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Repository;

use Admin\Entity\User;
use Doctrine\ORM\EntityRepository;

class PresentationRepository extends EntityRepository
{
    /**
     * Reseta todas as apresentações do usuário logado para "Não Executável" para inicializar outra apresentação
     * Com isso, sempre apenas uma apresentação fica sendo executada.
     * @param User $user
     * @return bool
     */
    public function updatePresentationToNotRunnableByUser(User $user)
    {
        $qb = $this->createQueryBuilder('p')
                   ->update('Admin\Entity\Presentation', 'p')
                   ->set('p.runnable', 'FALSE')
                   ->where('p.user = :user')
                   ->setParameter('user', $user);

        $qb->getQuery()->execute();
        return true;
    }
}