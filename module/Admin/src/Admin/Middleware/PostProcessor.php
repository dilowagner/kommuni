<?php
/**
 * Class PostProcessor
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Middleware;

use Admin\Interfaces\UserProviderInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Admin\Adapter\AuthAdapter;
use Zend\Mvc\MvcEvent;

class PostProcessor
{
    /**
     * @param MvcEvent $e
     * @throws \Exception
     */
    public function processUser(MvcEvent $e)
    {
        /** @var AbstractActionController|UserProviderInterface $controller */
        $controller = $e->getTarget();
        if($controller instanceof UserProviderInterface) {
            $sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
            $authService = new AuthenticationService();
            $authService->setStorage($sessionStorage);
            try {
                if($authService->hasIdentity()) {
                    $loggedUser = $authService->getIdentity();
                    $controller->setUser($loggedUser);
                }
            } catch(\Exception $e) {
                throw $e;
            }
        }
    }
}