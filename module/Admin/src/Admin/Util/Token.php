<?php
/**
 * Class Token
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Util;

use Hashids\Hashids;

class Token
{
    /**
     * @var string
     */
    private $token;

    public function __construct()
    {
        $hash  = new Hashids('kommuni', 100);
        $time  = floor((int)time()/2);
        $this->token = $hash->encode($time);
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    function __toString()
    {
        return $this->token;
    }
}