<?php
namespace Admin\Util;


/**
 * Class util para manipulacão de arquivos e diretórios.
 * 
 * @author AndréLuiz
 *
 */
class Files {
	
	
	/**
	 * @method removeDirectory
	 * Remove o diretório e todo o seu conteudo incluindo pastas e arquivos.
	 *
	 * @param string $dir
	 *
	 * @author Andre Luiz Haag
	 */
	public function removeDirectory($dir) {
		$abreDir = opendir($dir);
	
		while (false !== ($file = readdir($abreDir))) {
			if ($file==".." || $file ==".") continue;
			if (is_dir($cFile=($dir."/".$file))) $this->removeDirectory($cFile);
			elseif (is_file($cFile)) unlink($cFile);
		}
			
		closedir($abreDir);
		rmdir($dir);
	}
	
}