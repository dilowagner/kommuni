<?php
/**
 * Class LoginAwareInterface
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Interfaces;

interface LoginAwareInterface
{
    /**
     * @return mixed
     */
    public function loginAction();

    /**
     * @return mixed
     */
    public function logoutAction();
}