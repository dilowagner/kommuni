<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Interfaces;

use Admin\Entity\User;

interface UserProviderInterface
{
    /**
     * @param User $user
     */
    public function setUser(User $user);

    /**
     * @return User
     */
    public function getUser();
}