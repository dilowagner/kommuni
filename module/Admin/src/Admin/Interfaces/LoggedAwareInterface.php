<?php
/**
 * Class LoggedAwareInterface
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Interfaces;

interface LoggedAwareInterface
{
    /**
     * @return String
     */
    public function getMail();

    /**
     * @param $mail
     * @return String
     */
    public function setMail($mail);

    /**
     * @return String
     */
    public function getPassword();

    /**
     * @param $senha
     * @return String
     */
    public function setPassword($senha);

    /**
     * @return String
     */
    public function getSalt();

    /**
     * @param $salt
     * @return String
     */
    public function setSalt($salt);
} 