<?php
/**
 * Class SenhaUsuarioExists
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Validator;

use Admin\Entity\User;
use DoctrineModule\Validator\NoObjectExists;
use Zend\Validator\Exception\InvalidArgumentException;

class UserPasswordExists extends NoObjectExists
{
    /**
     * Error constants
     */
    const ERROR_OBJECT_FOUND    = 'objectFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_OBJECT_FOUND  => "Senha incorreta",
    );

    /**
     * @var User
     */
    private $user;

    public function __construct(array $options)
    {
        if(! isset($options['user']))
            throw new InvalidArgumentException("Não foi possível recuperar a instância do Usuário");

        $this->user = $options['user'];
        parent::__construct($options);
    }

    /**
     * {@inheritDoc}
     */
    public function isValid($value)
    {
        $value = $this->cleanSearchValue($value);
        $value['id']      = $this->user;
        $value['password']= $this->user->encryptPassword($value['password']);

        $match = $this->objectRepository->findOneBy($value);

        if (is_object($match)) {
            return true;
        }
        $this->error(self::ERROR_OBJECT_FOUND, $value);
        return false;
    }
}
