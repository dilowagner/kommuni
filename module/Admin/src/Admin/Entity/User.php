<?php
namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

use Admin\Interfaces\ObjectEntity;
use Admin\Interfaces\LoggedAwareInterface;
use Zend\Math\Rand,
    Zend\Crypt\Key\Derivation\Pbkdf2;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Admin\Repository\UserRepository")
 */
class User implements ObjectEntity, LoggedAwareInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50, nullable=false)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    protected $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=70, nullable=false)
     */
    protected $mail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="smallint", nullable=true, options={"default"=1})
     */
    protected $active = 1;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

	public function __construct(array $options = array())
    {
    	$this->created = new \DateTime("now");
    	$this->updated = new \DateTime("now");
        $this->salt = base64_encode(Rand::getBytes(8, true));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $this->encryptPassword($password);
    	return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * @method encryptPassword()
     * @param  string $password
     * @return string
     */
    public function encryptPassword($password)
    {
    	return base64_encode(Pbkdf2::calc('sha256', $password, $this->salt, 10000, strlen($password*2)));
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/user/';
        return $folders;
    }
}
