<?php
namespace Admin\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Admin\Interfaces\ObjectEntity;

/**
 * Presentation
 *
 * @ORM\Table(name="presentation")
 * @ORM\Entity(repositoryClass="Admin\Repository\PresentationRepository")
 */
class Presentation implements ObjectEntity
{
    const PATH_DOWNLOAD = './data/presentation/';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    private $html;

    /**
     * @var string
     *
     * @ORM\Column(name="forbidden_words", type="text", nullable=true)
     */
    private $forbiddenWords;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="text", nullable=true)
     */
    private $token;

    /**
     * @var boolean
     *
     * @ORM\Column(name="runnable", type="boolean", nullable=false, options={"default"=0})
     */
    private $runnable = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="poll_slide_increment", type="integer", nullable=false, options={"default"=0})
     */
    private $pollSlideIncrement = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="button_poll_increment", type="integer", nullable=false, options={"default"=0})
     */
    private $buttonPollIncrement = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="download_pdf", type="integer", nullable=false, options={"default"=0})
     */
    private $downloadPdf = 0;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Report", mappedBy="presentation", cascade={"remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $reports;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;;
    }

    /**
     * @return int
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $html
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return string
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param string $forbiddenWords
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setForbiddenWords($forbiddenWords)
    {
        $this->forbiddenWords = $forbiddenWords;
        return $this;
    }

    /**
     * @return string
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getForbiddenWords()
    {
        return $this->forbiddenWords;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRunnable()
    {
        return $this->runnable;
    }

    /**
     * @param boolean $runnable
     */
    public function setRunnable($runnable)
    {
        $this->runnable = $runnable;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * @param string $pollSlideIncrement
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setPollSlideIncrement($pollSlideIncrement)
    {
        $this->pollSlideIncrement = $pollSlideIncrement;
        return $this;
    }

    /**
     * @return string
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getPollSlideIncrement()
    {
        return $this->pollSlideIncrement;
    }

    /**
     * @param string $buttonPollIncrement
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setButtonPollIncrement($buttonPollIncrement)
    {
        $this->buttonPollIncrement = $buttonPollIncrement;
        return $this;
    }

    /**
     * @return string
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getButtonPollIncrement()
    {
        return $this->buttonPollIncrement;
    }

    /**
     * @param integer $downloadPdf
     * @return Presentation
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function setDownloadPdf($downloadPdf)
    {
        $this->downloadPdf = $downloadPdf;
        return $this;
    }

    /**
     * @return integer
     * @author AndreLuizHaag <andreluizhaag@gmail.com>
     */
    public function getDownloadPdf()
    {
        return $this->downloadPdf;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public/uploads/files/presentation_capa/';
        $folders[] = 'public/uploads/files/presentation/';
        return $folders;
    }

    /**
     * Utilizado para permitir a hidratação do form
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
