<?php
/**
 * Entity ReportType
 * Entidade - Tipo de relatório
 */
namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Admin\Interfaces\ObjectEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ReportRegistry
 *
 * @ORM\Table(name="report_registry",
 *     indexes={
 *          @ORM\Index(name="fk_report_registry_idx", columns={"report_id"})
 *     })
 * )
 * @ORM\Entity
 */
class ReportRegistry implements ObjectEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="registry", type="text", nullable=true)
     */
    private $registry;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $type;

    /**
     * @var Report
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="records")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $report;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * @param string $registry
     */
    public function setRegistry($registry)
    {
        $this->registry = $registry;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport(Report $report)
    {
        $this->report = $report;
    }
}