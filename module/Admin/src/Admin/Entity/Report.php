<?php
/**
 * Entity Report
 * Entidade - relatório das apresentações
 */
namespace Admin\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Admin\Interfaces\ObjectEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Report
 *
 * @ORM\Table(name="report",
 *     indexes={
 *          @ORM\Index(name="fk_report_presentation_idx", columns={"presentation_id"})
 *     })
 * @ORM\Entity
 */
class Report implements ObjectEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="local", type="string", length=50, nullable=true)
     */
    private $local;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_people", type="integer", nullable=true)
     */
    private $amountPeople;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var Presentation
     * @ORM\ManyToOne(targetEntity="Presentation", inversedBy="reports")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="presentation_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $presentation;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="ReportRegistry", mappedBy="report", cascade={"remove"}, orphanRemoval=true)
     */
    protected $records;


    public function __construct()
    {
        $this->records = new ArrayCollection();
        $this->created = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param string $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return string
     */
    public function getAmountPeople()
    {
        return $this->amountPeople;
    }

    /**
     * @param string $amountPeople
     */
    public function setAmountPeople($amountPeople)
    {
        $this->amountPeople = $amountPeople;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return Presentation
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * @param Presentation $presentation
     */
    public function setPresentation(Presentation $presentation)
    {
        $this->presentation = $presentation;
    }

    /**
     * @return Collection
     */
    public function getRecords()
    {
        return $this->records;
    }
}