<?php
/**
 * AbstractEnum
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Enum;

abstract class AbstractEnum
{
    /**
     * @var array
     */
    protected static $data;

    /**
     * @param $id
     * @return array
     */
    public static function get($id)
    {
       return static::$data[$id];
    }

    /**
     * @return array
     */
    public static function getArray()
    {
        return static::$data;
    }
} 