<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Enum;

class ReportRegistryType extends AbstractEnum
{
    const TYPE_POLL     = 'E';
    const TYPE_QUESTION = 'Q';

    public static $data = [
        self::TYPE_POLL     => 'Enquetes',
        self::TYPE_QUESTION => 'Perguntas'
    ];
}