<?php
/**
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form;

use Admin\Entity\User as UserEntity;
use Admin\Form\Fieldset\ChangePasswordFieldset;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class ChangePassword extends Form
{
    public function init()
    {
        parent::__construct('change-password');
        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form',
            'role'   => 'form'
        ));

        $this->add(array(
            'type'     => ChangePasswordFieldset::class,
            'name'     => 'user',
            'hydrator' => 'DoctrineModule\Stdlib\Hydrator\DoctrineObject',
            'object'   => UserEntity::class,
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
    }
} 