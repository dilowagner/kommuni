<?php
/**
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\Fieldset;

use Admin\Form\InputFilter\ChangePasswordFilter;
use Zend\Form\Element\Password;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class ChangePasswordFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function init()
    {
        $currentPassword = new Password('currentPassword');
        $currentPassword->setLabel('Senha Atual: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha Atual',
                'title'			=> 'Senha atual',
                'id'            => 'senha-atual',
                'maxlength'		=> '50'
            ));
        $this->add($currentPassword);

        $password = new Password('password');
        $password->setLabel('Nova Senha: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha',
                'title'			=> 'Senha que será utilizada para acessar o carrinho',
                'id'            => 'senha',
                'maxlength'		=> '50'
            ));
        $this->add($password);

        $confirmation = new Password('confirmation');
        $confirmation->setLabel('Confirmação: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Confirme a senha',
                'title'			=> 'Confirme a senha',
                'id'            => 'confirmacao',
                'maxlength'		=> '50'
            ));
        $this->add($confirmation);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => ChangePasswordFilter::class);
    }
} 