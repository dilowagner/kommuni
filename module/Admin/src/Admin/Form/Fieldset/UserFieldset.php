<?php
namespace Admin\Form\Fieldset;

use Admin\Form\InputFilter\UserFilter;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class UserFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function init()
    {
        // id
        $id = new Hidden('id');
        $this->add($id);

        $name = new Text('name');
        $name->setLabel('Nome: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Nome do usuário',
                'title'			=> 'Nome do usuário',
                'maxlength'		=> '45',
                'id'            => 'name'
            ));
        $this->add($name);

        $mail = new Text('mail');
        $mail->setLabel('E-mail: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Email',
                'title'			=> 'E-mail que será utilizado para o login e recuperação de senha',
                'maxlength'		=> '70',
                'id'            => 'mail'
            ));
        $this->add($mail);

        $active = new Checkbox('active');
        $active
            ->setAttributes(array(
                'class'			=> '',
                'data-placement'=> 'bottom',
                'title'			=>'Ativa ou desativa o produto',
                'value'         => 1,
                'id'            => 'active'
            ));
        $this->add($active);

        $password = new Password('password');
        $password->setLabel('Senha: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Senha',
                'title'			=> 'Senha de acesso ao painel',
                'maxlength'		=> '50',
                'id'            => 'password'
            ));
        $this->add($password);

        $confirmation = new Password('confirmation');
        $confirmation->setLabel('Confirmação: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip form-control',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Confirme a senha',
                'title'			=> 'Confirme a senha',
                'maxlength'		=> '50',
                'id'            => 'confirmation'
            ));
        $this->add($confirmation);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => UserFilter::class);
    }
}
