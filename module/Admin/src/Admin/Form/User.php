<?php
namespace Admin\Form;

use Admin\Form\Fieldset\UserFieldset;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;
use Admin\Entity\User as UserEntity;

class User extends Form
{
	public function init()
    {
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('role', 'form');

        $this->add(array(
            'type'     => UserFieldset::class,
            'name'     => 'user',
            'hydrator' => 'DoctrineModule\Stdlib\Hydrator\DoctrineObject',
            'object'   => UserEntity::class,
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
	}
}
