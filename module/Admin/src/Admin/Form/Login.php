<?php
namespace Admin\Form;

use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Admin\Form\InputFilter\LoginFilter;

class Login extends Form
{
	public function __construct($name = null, $options = array())
    {
		parent::__construct('login', $options);
		$this->setInputFilter(new LoginFilter());
        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form'
        ));

        $email = new Text('mail');
        $email->setAttributes(
            array(
                'class'			=> 'form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'E-mail',
                'title'			=> 'Email cadastrado com a discovery tecnologia.',
                'id'            => 'email',
                'maxlength'		=> '255'
            )
        );
        $this->add($email);

        $senha = new Password('password');
        $senha->setAttributes(
            array(
                'class'			=> 'form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha',
                'title'			=> 'Sua senha',
                'id'            => 'senha',
                'maxlength'		=> '50'
            )
        );
        $this->add($senha);
	}
}
