<?php
namespace Admin\Form;

use Zend\Form\Element\Hidden;
use Zend\Form\Form;

class Presentation extends Form
{
	public function __construct($name = null, $options = array())
    {
		parent::__construct('presentation', $options);

        // id
        $id = new Hidden('id');
        $id->setAttributes(
            array(
                'id' => 'hidden_id'
            )
        );
        $this->add($id);

        // name
        $name = new Hidden('name');
        $name->setAttributes(
            array(
                'id' => 'hidden_name'
            )
        );
        $this->add($name);

        // html
        $html = new Hidden('html');
        $html->setAttributes(
            array(
                'id' => 'hidden_html'
            )
        );
        $this->add($html);

        // forbidden_words
        $forbiddenWords = new Hidden('forbiddenWords');
        $forbiddenWords->setAttributes(
            array(
                'id' => 'hidden_forbidden_words'
            )
        );

        // poll_slide_increment
        $pollSlideIncrement = new Hidden('pollSlideIncrement');
        $pollSlideIncrement->setAttributes(
            array(
                'id' => 'hidden_poll_slide_increment'
            )
        );
        $this->add($pollSlideIncrement);

        // button_poll_increment
        $buttonPollIncrement = new Hidden('buttonPollIncrement');
        $buttonPollIncrement->setAttributes(
            array(
                'id' => 'hidden_button_poll_increment'
            )
        );
        $this->add($buttonPollIncrement);

        // download_pdf
        $downloadPdf = new Hidden('downloadPdf');
        $downloadPdf->setAttributes(
            array(
                'id' => 'hidden_download_pdf'
            )
        );
        $this->add($downloadPdf);



        $this->add($forbiddenWords);
	}
}
