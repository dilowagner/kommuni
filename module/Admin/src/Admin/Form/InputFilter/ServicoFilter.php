<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;


class ServicoFilter extends InputFilter {
	
	
	public function init() {

        // nome ###################### NOME (TEXT) ##################################
        $this->add(array(
            'name' => 'nome',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 100
                    ),
                )
            )
        ));

        // descricao #################### DESCRICAO (TEXT) ###########################
        $this->add(array(
            'name' => 'descricao',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));
	}
}