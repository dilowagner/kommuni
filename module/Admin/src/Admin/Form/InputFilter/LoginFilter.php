<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter
{
	public function __construct()
    {
		$this->add([
			'name' => 'mail',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim'],
			],
			'validators' => array(
				['name' => 'NotEmpty'],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 5,
                        'max'      => 255
                    ]
                ]
			)
		]);
		
		$this->add([
			'name' => 'password',
			'required' => true,
			'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
			],
			'validators' => [
				['name' => 'NotEmpty']
			]
		]);
	}
}