<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 27/03/14
 * Time: 21:22
 */

namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class SuporteFilter extends InputFilter {


    public function __construct() {

        // nome ###################### NOME (TEXT) ##################################
        $this->add(array(
            'name' => 'assunto',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 150
                    ),
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));

        // descricao ################ DESCRICAO (TEXT) ##########################
        $this->add(array(
            'name' => 'mensagem',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));
    }
} 