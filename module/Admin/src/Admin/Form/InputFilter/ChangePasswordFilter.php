<?php
/**
 * Class AlterarSenhaFilter
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\InputFilter;

use Admin\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Zend\InputFilter\InputFilter;

class ChangePasswordFilter extends InputFilter
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $objectRepository;

    /**
     * @var User
     */
    private $user;

    public function __construct(ObjectRepository $repository, User $user)
    {
        $this->objectRepository = $repository;
        $this->user = $user;
    }

    /**
     * @method init
     */
    public function init()
    {
        $this->add(array(
            'name' => 'currentPassword',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'UserPasswordExists',
                    'options' => array(
                        'object_repository' => $this->objectRepository,
                        'fields' => array('password'),
                        'user'   => $this->user
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'password',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));

        $this->add(array(
            'name' => 'confirmation',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'identical',
                    'options' => array(
                        'token' => 'password'
                    )
                )
            )
        ));
    }
} 