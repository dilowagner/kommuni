<?php
/**
 * Class AlterarSenhaFilterFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\InputFilter\Factory;

use Admin\Entity\User;
use Doctrine\ORM\EntityManager;
use Admin\Form\InputFilter\ChangePasswordFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Admin\Adapter\AuthAdapter;

class ChangePasswordFilterFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get(EntityManager::class);
        $objectRepository= $objectManager->getRepository(User::class);

        $sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
        $authService = new AuthenticationService();
        $authService->setStorage($sessionStorage);

        $user = $authService->getIdentity();
        return new ChangePasswordFilter($objectRepository, $user);
    }
} 