<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\InputFilter\Factory;

use Admin\Entity\User;
use Admin\Form\InputFilter\UserFilter;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserFilterFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return UserFilter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $locator          = $serviceLocator->getServiceLocator();
        /** @var ObjectManager $objectManager */
        $objectManager    = $locator->get(EntityManager::class);
        $objectRepository = $objectManager->getRepository(User::class);

        return new UserFilter($objectManager, $objectRepository);
    }
}