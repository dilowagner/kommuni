<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class ParceiroFilter extends InputFilter
{
	
	public function init() {
		
		// nome ########################## NOME (TEXT) ################################
		$this->add(array(
            'name' => 'nome',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => 70
                    )
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
		));

        // link ########################## LINK (TEXT) ################################
        $this->add(array(
            'name' => 'link',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 200
                    )
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));

        $this->add(array(
            'name'     => 'cliente',
            'required' => false
        ));

        $this->add(array(
            'name'     => 'parceiro',
            'required' => false
        ));

	}
	
}