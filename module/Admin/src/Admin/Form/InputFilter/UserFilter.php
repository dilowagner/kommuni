<?php
namespace Admin\Form\InputFilter;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Zend\InputFilter\InputFilter;

class UserFilter extends InputFilter
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * @param ObjectManager $objectManager
     * @param ObjectRepository $objectRepository
     */
    public function __construct(ObjectManager $objectManager, ObjectRepository $objectRepository)
    {
        $this->objectManager    = $objectManager;
        $this->objectRepository = $objectRepository;
    }

    public function init()
    {
		$this->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                ['name' => 'NotEmpty']
            ]
		]);

        $this->add([
            'name'     => 'active',
            'required' => false
        ]);

		$this->add([
            'name' => 'mail',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                ['name' => 'NotEmpty'],
                ['name' => 'EmailAddress'],
                [
                    'name' => 'DoctrineModule\Validator\UniqueObject',
                    'options' => [
                        'use_context'       => true,
                        'object_manager'    => $this->objectManager,
                        'object_repository' => $this->objectRepository,
                        'fields'            => 'mail',
                        'message'           => 'Este e-mail já está cadastrado.'
                    ]
                ]
            ]
		]);

		$this->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                ['name' => 'NotEmpty']
            ]
		]);

		$this->add([
            'name' => 'confirmation',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim']
            ],
            'validators' => [
                ['name' => 'NotEmpty'],
                [
                    'name' => 'identical',
                    'options' => [
                        'token' => 'password'
                    ]
                ]
            ]
		]);
	}
}