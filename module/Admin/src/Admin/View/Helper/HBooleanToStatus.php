<?php
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HBooleanToStatus extends AbstractHelper {
	
	public function __invoke($bool) {
		
		if($bool)
			return '<div class="grid_status sim" title="Sim"><span>Sim</span></div>';
		else
			return '<div class="grid_status nao" title="Não"><span>Não</span></div>';			
	}	
	
}