<?php
namespace Admin\View\Helper;

use Base\Upload\Images;

use Zend\View\Helper\AbstractHelper;

class HImages extends AbstractHelper {
	
	public function __invoke($path, $id) {
		
		// imagens
		$imgs = new Images($path, $id);
		$infoImages = array(
				'img' => $imgs->findFirst(),
				'imgs' => $imgs->findAll(),
				'imgPath'=>$imgs->getImgPath(),
				'thumbnailPath'=>$imgs->getThumbnailPath()
		);			
		
		return $infoImages;
	}	
	
}