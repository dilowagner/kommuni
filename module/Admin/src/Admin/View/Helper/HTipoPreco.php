<?php
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HTipoPreco extends AbstractHelper {

	
	public function __invoke($char) {
	
	    if($char == 'P')
	       $tipoPreço = 'Por: ';
	    else 
	        $tipoPreço = 'Á partir de: ';
	        
		return $tipoPreço;
	}
	
	
}