<?php
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService,
	Zend\Authentication\Storage\Session as SessionStorage;
use Admin\Adapter\AuthAdapter;

class UserIdentity extends AbstractHelper
{
	public function __invoke()
    {
		$sessionStorage = new SessionStorage(AuthAdapter::USER_SESSION);
		$authService = new AuthenticationService();
		$authService->setStorage($sessionStorage);
		
		if($authService->hasIdentity())
			return $authService->getIdentity();
		else
			return false;
	}
}