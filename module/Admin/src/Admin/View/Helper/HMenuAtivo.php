<?php 
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HMenuAtivo extends AbstractHelper
{
	protected $routeMatch;
	
        	
    public function __construct($routeMatch)
    {
    	$this->routeMatch = $routeMatch;
    }
        	
    public function __invoke($url)
    {
    	$routeName = str_replace('-paginator', '', $this->routeMatch);
    	$routeName = str_replace('/default', '', $routeName);
    	$routeName = str_replace('/view', '', $routeName);
    	$routeName = str_replace('/filter', '', $routeName);
    	
    	if ($routeName == $url) {
        	return 'ativo';
        } else {
        	return '';
        }
  	}
}
