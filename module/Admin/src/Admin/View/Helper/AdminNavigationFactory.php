<?php
namespace Admin\View\Helper;

use Zend\Navigation\Service\AbstractNavigationFactory;

class AdminNavigationFactory extends AbstractNavigationFactory {
    
    
    /**
     * @method getName()
     * Retorna o nome do navigation registrado no global.php
     * (non-PHPdoc)
     * @see \Zend\Navigation\Service\AbstractNavigationFactory::getName()
     */
    public function getName() {

        return 'menu_admin';
    }
}