<?php 
namespace Admin\View\Helper;


use Site\Service\EnderecoService;
use Zend\View\Helper\AbstractHelper;


class HEnderecoByCep extends AbstractHelper {

    /**
     * @var EnderecoService
     */
    protected $service;



    /**
     * Dependency Injection
     * @param EnderecoService $enderecoService
     */
    public function __construct( EnderecoService $enderecoService ) {

        $this->service = $enderecoService;

    }
    


    public function __invoke( $cep ) {

        $endereco =  $this->service->findOneEnderecoByCep( $cep );

        return $endereco;
       
  	}       	
    
       
}