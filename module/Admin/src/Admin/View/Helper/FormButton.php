<?php
namespace Admin\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormButton as ZendFormButton;
/**
 * Sobrescrita da helper FormButton do zend para
 * permitir a insecao de icones sem dentro da tag sem escape
 * de HTML
 * 
 * @author AndréLuiz
 *
 */
class FormButton extends ZendFormButton {

	public function render(ElementInterface $element, $buttonContent = null)
	{
		$content = (isset($buttonContent)) ? $buttonContent : $element->getLabel();
		
		//$icon    = isset($element->getOption('icon')) ? $element->getOption('icon') : '';
		
		if($element->getOption('icon'))
		    $icon    = $element->getOption('icon');
		else
		    $icon    = '';		

		$escape = $this->getEscapeHtmlHelper();

		return $this->openTag($element) . $icon . $escape($content) . $this->closeTag();
	}

}