<?php
/**
 * Class HMaskTelefone
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\View\Helper;


use Zend\View\Helper\AbstractHelper;

class HMaskTelefone extends AbstractHelper
{

    /**
     * @var CommonMask
     */
    private $commonMask;


    function __construct(HCommonMask $commonMask)
    {
        $this->commonMask = $commonMask;
    }


    public function __invoke($number)
    {
        if (!$number === true) {
            return '--';
        }
        return $this->commonMask->mask($number, '%2s%4s%5s', '(%2s) %4s-%5s');
    }
} 