<?php
/**
 * Class HMaskCpfCnpjFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Base\View\Helper\Factory;

use Base\View\Helper\HMaskCpfCnpj;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HMaskCpfCnpjFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $serviceLocator = $services->getServiceLocator();
        $helpers        = $serviceLocator->get('ViewHelperManager');
        $commonMask     = $helpers->get('Base\View\Helper\HCommonMask');

        return new HMaskCpfCnpj($commonMask);
    }
} 