<?php 
namespace Admin\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Admin\View\Helper\HMenuAtivo;

class HMenuAtivoFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $sm = $serviceLocator->getServiceLocator();
        $routeName = $sm->get('application')->getMvcEvent()->getRouteMatch()->getMatchedRouteName();

        return new HMenuAtivo($routeName);

    }
    
       
}