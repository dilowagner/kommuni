<?php 
namespace Base\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

use Base\View\Helper\HEnderecoByCep;


class HEnderecoByCepFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $serviceLocator = $serviceLocator->getServiceLocator();

        $enderecoService    = $serviceLocator->get('Site\Service\Endereco');

        return new HEnderecoByCep( $enderecoService );

    }
       
}