<?php 
namespace Base\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Base\View\Helper\HAbsoluteUrl;

/**
 * est ViewHelper retorna a URL absoluta
 * 
 * @author AndréLuiz
 *
 */
class HAbsoluteUrlFactory implements FactoryInterface {



    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $sm = $serviceLocator->getServiceLocator();


        return new HAbsoluteUrl($sm->get('Request'));

    }
}