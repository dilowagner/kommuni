<?php
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HPreco extends AbstractHelper {

	
	public function __invoke($preco) {
	
	    $array = explode('.', $preco);
	    $reais = $array[0];
	    $centavos = isset($array[1]) ? $array[1] : null;
	    
	    if($reais == 0 && $centavos == 00) {
	        
    	    $preco = 'Consulte';
	    
	    } else {
	    	
	        // se nao existir nada apos o ponto
	        if(!$centavos)
	        	$centavos = '00';
	        // se so existir um numero apos o ponto
	        elseif($centavos >= 1 && $centavos <= 9 )
	        $centavos = $centavos.'0';
	        	
	        $preco = '<span class="currency">R$</span> '.$reais.',<span class="cents">'.$centavos.'</span>';
	        
	    }    
	    
		return $preco;
	}
	
	
}