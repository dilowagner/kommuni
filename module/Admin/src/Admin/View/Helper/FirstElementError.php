<?php
/**
 * @author Diego Wagner
 */
namespace Admin\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\FormElementErrors;

class FirstElementError extends FormElementErrors
{
    /**
     * @var array
     */
    protected $attributes = array();

    /**
     * @param ElementInterface $element
     * @param array $attributes
     * @return null|string
     */
    public function render(ElementInterface $element, array $attributes = array())
    {
        $messages = $element->getMessages();
        if (empty($messages)) {
            return null;
        }
        $markup = sprintf('<div class="has-error">
                                <label class="%s">%s</label>
                            </div>',
            'error-block', reset($messages));
        return $markup;
    }
}
