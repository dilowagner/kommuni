<?php 
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FlashMessages extends AbstractHelper {

        protected $flashMessenger;

        public function setFlashMessenger($flashMessenger) {
        	$this->flashMessenger = $flashMessenger ;
        }

        public function __invoke() {

        	$types = array( 
            	'error',
                'success', 
                'info',
                'alert' 
            );

            // messages as string
            $messageString = '';
            
            $messages = $this->flashMessenger->getMessages();

            foreach($types as $ty) {

                if ( empty( $messages ) )
                	continue;
                
                if ( !isset($messages[0][$ty]) || empty($messages[0][$ty]) )
                	continue;

                if ( is_array( $messages[0][$ty] ) ) {
                	$message = '';
                	foreach ( $messages[0][$ty] as $row ) {
                		$message .= $row . '<br />';
                	}
	                
		                $messageString .= "<div class='alert alert-{$ty}'>"
		                				 ."<button type='button' class='close' data-dismiss='alert' >x</button>"
		                                 . $message
		                                 .'</div>';
                    
                } else {
                	
                	    $messageString .= "<div class='alert alert-{$ty}'>"
				                         ."<button type='button' class='close' data-dismiss='alert' >x</button>"
				                	     . $messages[0][$ty]
				                	     .'</div>';
                }
            }
            
            unset( $messages );

            return $messageString ;
        }
}