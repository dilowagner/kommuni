<?php
namespace Admin;

return [
    /** ROUTES MANAGER *********************************************************/
    'router' => [
        'routes' => [
            # LOGIN #############################################################
            'admin-login' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/admin/login',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Login',
                        'action'     => 'login',
                    ],
                ],
            ],
            # ACCOUNT ###########################################################
            'admin-account' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/admin/account',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Account',
                        'action'     => 'index',
                    ],
                ],
            ],
            'admin-logout' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/admin/logout',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Login',
                        'action'     => 'logout',
                    ],
                ],
            ],
            # INDEX #############################################################
            'admin-index' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/admin',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
            ],
            # PRESENTATION #######################################################
            'admin-presentation' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/presentation',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Presentation',
                        'action' => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'defaults' => [
                                    'controller' => 'Admin\Controller\Presentation'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            # ESTATISTICS #######################################################
            'admin-statistics' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/statistics',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Statistics',
                        'action' => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'defaults' => [
                                    'controller' => 'Admin\Controller\Statistics'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            # USER ###############################################################
            'admin-user' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/admin/user',
                    'defaults' => [
                        'controller' => 'Admin\Controller\User',
                        'action'     => 'index',
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '[/:action]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => [
                                'controller' => 'Admin\Controller\User'
                            ]
                        ]
                    ]
                ]
            ],
            # INDEX #############################################################
            'admin-help' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/admin/help',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Help',
                        'action'     => 'index',
                    ],
                ],
            ],
        ]
    ],
    /** CONTROLLERS *******************************************/
    'controllers' => [
        'invokables' => [
            'Admin\Controller\Index'   	     => 'Admin\Controller\IndexController',
            'Admin\Controller\Login'   	     => 'Admin\Controller\LoginController',
            'Admin\Controller\Help'   	     => 'Admin\Controller\HelpController',
        ],
        'factories' => [
            'Admin\Controller\Account'   	 => 'Admin\Controller\Factory\AccountControllerFactory',
            'Admin\Controller\Presentation'  => 'Admin\Controller\Factory\PresentationControllerFactory',
            'Admin\Controller\Statistics'    => 'Admin\Controller\Factory\StatisticsControllerFactory',
            'Admin\Controller\User'          => 'Admin\Controller\Factory\UserControllerFactory'
        ],
        'abstract_factories' => [
            'Admin\Controller\Factory\AbstractControllerFactory'
        ]
    ],
    /** VIEW MANAGERS ******************************************/
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        #'exception_template'      => 'admin/error/index',
        'template_map' => [
            'layout/admin'                         => __DIR__ . '/../view/layout/admin.phtml',
            'layout/login'                         => __DIR__ . '/../view/layout/login.phtml',
            'admin/index/index'                    => __DIR__ . '/../view/admin/index/index.phtml',
            'admin/error/index'                    => __DIR__ . '/../view/error/index.phtml',
            'admin/partial/upload'                 => __DIR__ . '/../view/partial/file-upload.phtml',
            'admin/partial/dlg-presentation-start' => __DIR__ . '/../view/partial/dlg-presentation-start.phtml'
        ],
        'template_path_stack' => [
            'admin' => __DIR__ . '/../view',
        ],
    	'strategies' => [
    		'ViewJsonStrategy'
    	]
    ],
    /** SERVICE MANAGERS **************************************/
    'service_manager' => [
        'factories' => [
            'Navigation\Admin'           => 'Admin\View\Helper\AdminNavigationFactory',
            'Admin\Service\Presentation' => 'Admin\Service\Factory\PresentationServiceFactory',
            'admin.service.user'         => 'Admin\Service\Factory\UserServiceFactory',
            'admin.auth.adapter'         => 'Admin\Service\Factory\AdapterServiceFactory',
        ]
    ],
    'view_helpers' => [
        'factories' => [
            'HMenuAtivo'     => 'Admin\View\Helper\Factory\HMenuAtivoFactory',
            'HAbsoluteUrl'   => 'Admin\View\Helper\Factory\HAbsoluteUrlFactory',
            'HEnderecoByCep' => 'Admin\View\Helper\Factory\HEnderecoByCepFactory',
            'HMaskCpfCnpj'   => 'Admin\View\Helper\Factory\HMaskCpfCnpjFactory',
            'HMaskTelefone'  => 'Admin\View\Helper\Factory\HMaskTelefoneFactory',
            'alertMessage'   => 'Admin\View\Helper\Factory\AlertMessageFactory'
        ],
        'invokables' => [
            'HStringToSlug'		=> 'Admin\View\Helper\HStringToSlug',
            'HStringToKeyWords'	=> 'Admin\View\Helper\HStringToKeyWords',
            'HImages' 			=> 'Admin\View\Helper\HImages',
            'HCommonMask'		=> 'Admin\View\Helper\HCommonMask',
            'HBooleanToStatus'	=> 'Admin\View\Helper\HBooleanToStatus',
            'HIntToAlertStatus'	=> 'Admin\View\Helper\HIntToAlertStatus',
            'form_button'       => 'Admin\View\Helper\FormButton',
            'UserIdentity'      => 'Admin\View\Helper\UserIdentity',
            'firstElementError' => 'Admin\View\Helper\FirstElementError'
        ]
    ],

    /** INPUT FILTERS **/
    'input_filters' => [
        'factories' => [
            'Admin\Form\InputFilter\ChangePasswordFilter' => 'Admin\Form\InputFilter\Factory\ChangePasswordFilterFactory',
            'Admin\Form\InputFilter\UserFilter'           => 'Admin\Form\InputFilter\Factory\UserFilterFactory'
        ]
    ],

    /** VALIDATORS */
    'validators' => [
        'invokables' => [
            'UserPasswordExists' => 'Admin\Validator\UserPasswordExists'
        ],
    ],

    'acl' => [
        'modules' => [
            'admin'
        ]
    ]
];
