<?php
/**
 * Class LoggerServiceFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Service\Factory;

use Log\Handling\Formatter;
use Log\Handling\Logger;
use Log\Handling\Writer;
use Log\Service\LoggerService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggerServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\ServiceManager\Config $config */
        $config = $serviceLocator->get('Config');
        $writer = new Writer($config);
        $writer = $writer->getWriter();
        $formatter = new Formatter($config);
        $writer->setFormatter($formatter->getFormatter());

        $log = new Logger();
        $log->addWriter($writer);

        return new LoggerService($log);
    }
} 