<?php
/**
 * Class Logger
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Handling;

use Zend\Log\Logger as ZendLogger;

class Logger extends ZendLogger
{
    const LOG_FILENAME  = 'log';
    const LOG_EXTENSION = '.txt';
    const LOG_PATH      = './data/logs/';
} 