<?php
/**
 * Class Stream
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Handling;

use Log\Exception\LoggerConfigException;
use Zend\Log\Writer\Stream;

class Writer extends LoggerConfig
{

    const MODE = 'r+';
    /**
     * @return Stream|\Zend\Log\Writer\WriterInterface
     * @throws \Log\Exception\LoggerConfigException
     */
    public function getWriter()
    {
        // Writer default
        if (!isset($this->config[static::CONFIG]['writer'])) {
            return new Stream(Logger::LOG_PATH . Logger::LOG_FILENAME . Logger::LOG_EXTENSION);
        }
        $config = $this->config[static::CONFIG];
        if (! isset($config['writer']['name'])) {
            throw new LoggerConfigException('Não foi possível recuperar o nome do Writer');
        }
        $format = $this->getFormat($config);
        $class = $config['writer']['name'];
        /** @var \Zend\Log\Writer\WriterInterface $writer */
        $writer = new $class($format);

        return $writer;
    }

    /**
     * @param $config
     * @return string
     */
    private function getFormat($config)
    {
        $options = $this->getOptions($config);
        return $options['path'] . $options['filename'];
    }
} 