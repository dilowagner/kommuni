<?php
/**
 * Class Module
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log;

use Log\Interfaces\LoggerAwareInterface;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $application = $e->getTarget();
        $services = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $sharedManager->attach('Zend\Mvc\Application', MvcEvent::EVENT_DISPATCH_ERROR,
            function(EventInterface $e) use ($services) {
                if ($e->getParam('exception')){
                    $exception = $e->getParam('exception');
                    /** @var \Log\Handling\Logger $service */
                    $service = $services->get('Log\Service\LoggerService');
                    $service->err($exception->getMessage());
                }
            }
        );

        $sharedManager->attach('Zend\Mvc\Application', MvcEvent::EVENT_RENDER_ERROR,
            function(EventInterface $e) use ($services) {
                if ($e->getParam('exception')){
                    $exception = $e->getParam('exception');
                    /** @var \Log\Handling\Logger $service */
                    $service = $services->get('Log\Service\LoggerService');
                    $service->err($exception->getMessage());
                }
            }
        );

        $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', MvcEvent::EVENT_DISPATCH, function(EventInterface $e) use($services){
            $controller = $e->getTarget();
            if ($controller instanceof LoggerAwareInterface) {
                $loggerService = $services->get('Log\Service\LoggerService');
                $controller->setLogger($loggerService);
            }
        }, 101);
    }
}