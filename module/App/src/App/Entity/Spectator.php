<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Spectator
 *
 * @ORM\Table(name="spectator")
 * @ORM\Entity(repositoryClass="App\Repository\SpectatorRepository")
 */
class Spectator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=70, nullable=false)
     */
    protected $nickname;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }
}
