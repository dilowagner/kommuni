<?php
/**
 * @author Diego Wagner diegowagner4@gmail.com
 */
namespace App\Session;

use Zend\Session\SaveHandler\DbTableGatewayOptions;

class DatabaseSessionConfig extends DbTableGatewayOptions
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $timeColumn;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->tableName      = isset($options['db_table']) ? $options['db_table'] : null;
        $this->idColumn       = isset($options['db_id_col']) ? $options['db_id_col'] : null;
        $this->dataColumn     = isset($options['db_data_col']) ? $options['db_data_col'] : null;
        $this->timeColumn     = isset($options['db_time_col']) ? $options['db_time_col'] : null;
        $this->lifetimeColumn = isset($options['db_lifetime_col']) ? $options['db_lifetime_col'] : null;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @return string
     */
    public function getTimeColumn()
    {
        return $this->timeColumn;
    }

    /**
     * @param string $timeColumn
     */
    public function setTimeColumn($timeColumn)
    {
        $this->timeColumn = $timeColumn;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'db_table'        => $this->tableName,
            'db_id_col'       => $this->idColumn,
            'db_data_col'     => $this->dataColumn,
            'db_time_col'     => $this->timeColumn,
            'db_lifetime_col' => $this->lifetimeColumn
        ];
    }
}