<?php
/**
 * Sobrescrita da Classe SessionProvider da biblioteca Ratchet.
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Session;

use Ratchet\Session\SessionProvider as RatchetSessionProvider;
use Ratchet\ConnectionInterface;

class SessionProvider extends RatchetSessionProvider
{
    /**
     * @param ConnectionInterface $conn
     * @return mixed
     */
    public function onOpen(ConnectionInterface $conn)
    {
        if (!isset($conn->WebSocket) || null === ($id = $conn->WebSocket->request->getCookie(ini_get('session.name')))) {
            $saveHandler = $this->_null;
            $id = '';
        } else {
            $saveHandler = $this->_handler;
        }

        $conn->Session = new CustomizedSession(new CustomizedVirtualSessionStorage($saveHandler, $id, $this->_serializer));
        if (ini_get('session.auto_start')) {
            $conn->Session->start();
        }
        return $this->_app->onOpen($conn);
    }
}