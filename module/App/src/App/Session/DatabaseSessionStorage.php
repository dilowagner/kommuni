<?php
/**
 * Sobrescrita da Classe SessionArrayStorage do Zend.
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Session;

use App\Enum\SessionName;
use Zend\Session\Exception\RuntimeException;
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Stdlib\ArrayObject;

class DatabaseSessionStorage extends SessionArrayStorage
{
    /**
     * @param string $key
     * @param mixed $value
     * @param bool $overwriteArray
     * @return \Zend\Session\Storage\ArrayStorage
     */
    public function setMetadata($key, $value, $overwriteArray = false)
    {
        $arrayStorage = parent::setMetadata($key, $value, $overwriteArray = false);
        $sessionName  = SessionName::SESSION_NAME;
        if(isset($_SESSION[$sessionName])) {
            if ($this->isImmutable()) {
                throw new RuntimeException(
                    sprintf('Cannot set key "%s" as storage is marked isImmutable', $key)
                );
            }
            $value = $_SESSION[$sessionName];
            if (isset($_SESSION[$sessionName]) && $value instanceof ArrayObject) {
                $_SESSION[$sessionName] = $value->getArrayCopy();
            }
        }

        return $arrayStorage;
    }
}