<?php
/**
 * Sobrescrita da Classe Session do Symfony.
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Session;

use App\Enum\SessionName;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

class CustomizedSession extends Session
{
    /**
     * @param SessionStorageInterface $storage
     * @param AttributeBagInterface $attributes
     * @param FlashBagInterface $flashes
     */
    public function __construct(SessionStorageInterface $storage = null, AttributeBagInterface $attributes = null, FlashBagInterface $flashes = null)
    {
        $attributes = new AttributeBag(SessionName::SESSION_NAME);
        parent::__construct($storage, $attributes, $flashes);
    }
}