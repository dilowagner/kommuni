<?php
/**
 * Sobrescrita da Classe VirtualSessionStorage do Symfony.
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Session;

use App\Enum\SessionName;
use Ratchet\Session\Storage\VirtualSessionStorage;
use Zend\Stdlib\ArrayObject;

class CustomizedVirtualSessionStorage extends VirtualSessionStorage
{
    /**
     * {@inheritdoc}
     */
    public function getBag($name)
    {
        if (!isset($this->bags[$name]) && $name != SessionName::SESSION_NAME) {
            throw new \InvalidArgumentException(sprintf('The SessionBagInterface %s is not registered.', $name));
        }

        if ($this->saveHandler->isActive() && !$this->started) {
            $this->loadSession();
        } elseif (!$this->started) {
            $this->start();
        }
        return $this->bags[$name];
    }

    /**
     * Load the session with attributes.
     *
     * After starting the session, PHP retrieves the session from whatever handlers
     * are set to (either PHP's internal, or a custom save handler set with session_set_save_handler()).
     * PHP takes the return value from the read() handler, unserializes it
     * and populates $_SESSION with the result automatically.
     *
     * @param array|null $session
     */
    protected function loadSession(array &$session = null)
    {
        if (null === $session) {
            $session = &$_SESSION;
        }

        $bags = array_merge($this->bags, array($this->metadataBag));

        foreach ($bags as $bag) {
            $key = $bag->getStorageKey();
            $session[$key] = isset($session[$key]) ? $session[$key] : array();
            $value = $session[$key];
            if($value instanceof ArrayObject) {
                $session[$key] = $value->getArrayCopy();
            }
            $bag->initialize($session[$key]);
        }

        $this->started = true;
        $this->closed = false;
    }
}