<?php
namespace App\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class LoginPresenterFilter extends InputFilter {

    public function __construct() {

        $this->add([
            'name' => 'mail',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ]
        ]);

        $this->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ]
        ]);
    }
}