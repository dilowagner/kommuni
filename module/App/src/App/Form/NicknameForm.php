<?php
/**
 * Class NicknameForm
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace App\Form;

use Zend\Form\Element\Text;
use Zend\Form\Form;

class NicknameForm extends Form
{
    public function __construct()
    {
        parent::__construct('nickname-form');
        $this->setAttribute('method', 'post');

        $nickname = new Text('nickname');
        $nickname->setLabel ( 'Apelido: ' )
            ->setLabelAttributes (array('class' => 'nickname'))
            ->setAttributes ( array (
                'class' => 'tool_tip',
                'data-placement' => 'bottom',
                'placeholder' => 'Seu apelido',
                'title' => 'Nome do cliente',
                'id' => 'nome',
                'maxlength' => '70'
            ) );
        $this->add ($nickname);
    }
} 