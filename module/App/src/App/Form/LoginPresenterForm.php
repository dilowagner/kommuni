<?php
/**
 * Class LoginPresenterForm
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace App\Form;

use App\Form\InputFilter\LoginPresenterFilter;
use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class LoginPresenterForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct('login', $options);
        $this->setInputFilter(new LoginPresenterFilter());
        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form form-horizontal'
        ));

        $mail = new Text('mail');
        $mail->setAttributes(array(
            'placeholder'	=> 'Digite seu e-mail',
            'title'			=> 'Digite seu e-mail',
            'id'            => 'mail',
            'maxlength'		=> '70'
        ));
        $this->add($mail);

        $passowrd = new Password('password');
        $passowrd->setAttributes(array(
            'placeholder'	=> 'Digite sua senha',
            'id'            => 'password',
            'maxlength'		=> '70'
        ));
        $this->add($passowrd);
    }
} 