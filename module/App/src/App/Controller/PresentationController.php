<?php
/**
 * Class PresentationController
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Controller;

use Admin\Controller\AbstractCrudController;
use Admin\Entity\Presentation;
use Admin\Exception\BusinessException;
use App\Enum\SessionName;
use App\Http\DownloadResponse;
use App\Service\PresentationService;
use Server\Service\WebsocketService;
use Zend\Http\Request;
use Zend\Mvc\Exception\BadMethodCallException;
use Zend\Session\Container;
use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class PresentationController extends AbstractCrudController
{
    /**
     * @var WebsocketService
     */
    private $websocket;

    /**
     * @var Container
     */
    private $session;

    /**
     * @param PresentationService $service
     * @param WebsocketService $websocket
     */
    public function __construct(PresentationService $service, WebsocketService $websocket)
    {
        $this->service   = $service;
        $this->websocket = $websocket;
        $this->session   = new Container(SessionName::SESSION_NAME);
    }

    /**
     * @return array|\Zend\Http\Response|ViewModel
     */
    public function indexAction()
    {
        $presenter = false;
        // verifica se existe o modo Apresentador
        $isPresenter = $this->session->offsetExists('presenter');
        if($isPresenter) {
            $presenter = true;
        }

        // verifica se já existe o nickname para o espectador
        $isNickname = $this->session->offsetExists('nickname');
        if(! $isNickname && ! $presenter) {
            return $this->redirect()->toRoute('app-nickname');
        }
        $host         = $this->websocket->getWebsocketHost();
        $presentation = $this->service->findPresentation();
        $reportId     = $this->session->offsetGet('report');

        return new ViewModel([
            'host'         => $host,
            'presenter'    => $presenter,
            'presentation' => $presentation,
            'report'       => $reportId
        ]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function printAction()
    {
        $logger = $this->getLogger();
        try {

            $id     = $this->params()->fromRoute('id', 0);
            $token  = $this->params()->fromRoute('token', 0);

            $presentation = $this->service->findOneBy(['id' => $id, 'token' => $token]);
            if (! ($presentation instanceof Presentation)) {
                $logger->alert(sprintf("Tentativa de acesso com o token: %s", $token));
                die('Token inválido para impressão da apresentação.');
            }
            $fileName = sprintf('%s.pdf', $presentation->getName());
            $path = Presentation::PATH_DOWNLOAD . $fileName;
            $file = new \SplFileInfo($path);
            if(! $file->isReadable()) {
                die('Não foi possível recuperar o arquivo.');
            }
            return new DownloadResponse($file, $fileName);

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            die('Houve um erro ao tentar realizar a impressão.');
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function previewAction()
    {
        $logger = $this->getLogger();
        try {

            $userId = $this->params()->fromRoute('user', 0);
            $token  = $this->params()->fromRoute('token', 0);
            $user   = $this->service->findUser($userId);
            $presentation = $this->service->findOneBy(['token' => $token, 'user' => $user]);
            if (! ($presentation instanceof Presentation)) {
                $logger->alert(sprintf("Tentativa de acesso com o token: %s", $token));
                $this->flashMessenger()->addWarningMessage('Token inválido para inicialização da apresentação.');
                return $this->redirect()->toRoute('admin-presentation');
            }

            if(! $presentation->isRunnable()) {
                $this->service->startPresentation($presentation, $user);
            }
            $this->session->offsetSet('presenter', true);
            return $this->redirect()->toRoute('app-presentation');

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            $this->session->offsetSet('presenter', false);
            $this->flashMessenger()->addErrorMessage($e->getMessage());
            return $this->redirect()->toRoute('admin-presentation');
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function startAction()
    {
        $logger = $this->getLogger();
        $return = [];
        try {

            /** @var Request $request */
            $request = $this->getRequest();
            if(! $request->isPost()) {
                throw new BadMethodCallException('Não foi possível recuperar os dados para iniciar a apresentação.');
            }

            /** @var Parameters $post */
            $post = $request->getPost();
            $user = $this->service->findUser($post->user);
            $presentation = $this->service->findOneBy(['token' => $post->token, 'user' => $user]);
            if (! ($presentation instanceof Presentation)) {
                $logger->alert(sprintf("Tentativa de acesso com o token: %s", $post->token));
                throw new BusinessException('Token inválido para inicialização da apresentação.');
            }

            $return['status'] = 'success';
            $this->service->startPresentation($presentation, $user);
            $report = $this->service->saveNewReport($presentation, $post);
            $this->session->offsetSet('presenter', true);
            $this->session->offsetSet('report', $report->getId());
            $this->service->savePdf($presentation);

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            $this->session->offsetSet('presenter', false);
            $return['status']  = 'error';
            $return['message'] = $e->getMessage();
        }
        return new JsonModel($return);
    }

    /**
     * @return JsonModel
     */
    public function finishAction()
    {
        $logger = $this->getLogger();
        $return = [];
        try {

            /** @var Request $request */
            $request = $this->getRequest();
            if(! $request->isPost()) {
                throw new BadMethodCallException('Não foi possível recuperar os dados para iniciar a apresentação.');
            }

            /** @var Parameters $post */
            $post = $request->getPost();
            $presentation = $this->service->findOneBy(['token' => $post->token, 'runnable' => true]);
            if (! ($presentation instanceof Presentation)) {
                $logger->alert(sprintf("Tentativa de acesso para reiniar a apresentação com o token: %s", $post->token));
                throw new \Exception();
            }
            $presentation->setRunnable(false);
            $this->service->clearAllSpectator();
            $this->service->save($presentation);
            $this->session->offsetUnset('nickname');
            $return['status'] = 'success';
            sleep(8);

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            $return['status']  = 'error';
            $return['message'] = $e->getMessage();
        }
        return new JsonModel($return);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function restartAction()
    {
        $logger = $this->getLogger();
        try {

            $token  = $this->params()->fromRoute('token', 0);
            $presentation = $this->service->findOneBy(['token' => $token, 'runnable' => true]);
            if (! ($presentation instanceof Presentation)) {
                $logger->alert(sprintf("Tentativa de acesso para reiniar a apresentação com o token: %s", $token));
                $this->flashMessenger()->addWarningMessage('Token inválido para inicialização da apresentação.');
                return $this->redirect()->toRoute('admin-presentation');
            }
            $this->session->offsetSet('presenter', true);
            return $this->redirect()->toRoute('app-presentation');

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            $this->session->offsetSet('presenter', false);
            $this->flashMessenger()->addErrorMessage($e->getMessage());
            return $this->redirect()->toRoute('admin-presentation');
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function saveReportAction()
    {
        $logger = $this->getLogger();
        $return = [];
        try {

            /** @var Request $request */
            $request = $this->getRequest();
            if(! $request->isPost()) {
                throw new BadMethodCallException('Não foi possível recuperar os dados para salvar o relatório da apresentação.');
            }

            /** @var Parameters $post */
            $post = $request->getPost();
            $report = $this->service->findReport($post->id);
            $registry = $this->service->saveReportRegistry($report, $post);
            $return['status']   = 'success';
            $return['registry'] = $registry->getId();

        } catch(\Exception $e) {
            $logger->err($e->getMessage());
            $return['status']  = 'error';
            $return['message'] = $e->getMessage();
        }
        return new JsonModel($return);
    }
} 