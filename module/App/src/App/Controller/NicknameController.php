<?php
/**
 * Class NicknameController
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Controller;

use App\Entity\Spectator;
use App\Enum\SessionName;
use App\Service\SpectatorService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class NicknameController extends AbstractActionController
{
    /**
     * @var SpectatorService
     */
    private $service;

    /**
     * @var Container
     */
    private $session;

    /**
     * @param SpectatorService $service
     */
    public function __construct(SpectatorService $service)
    {
        $this->service = $service;
        $this->session = new Container(SessionName::SESSION_NAME);
    }

    /**
     * @return array|JsonModel
     */
    public function indexAction()
    {
        $nickname = false;
        $request = $this->getRequest();
        if($request->isPost()) {

            $post     = $request->getPost();
            $nickname = $post->nickname;
            $spectator = $this->service->findSpectatorByNickname($nickname);
            if($spectator instanceof Spectator) {
                //caso o apelido exista, gera um número aleatório no final
                $nickname .= rand(1,9);
            }
            $this->session->offsetSet('nickname', $nickname);
            $spectator = new Spectator();
            $spectator->setNickname($nickname);
            $this->service->save($spectator);

            return new JsonModel([
                'message' => 'Seu apelido será: ' . $nickname,
                'success' => true
            ]);
        }
        return new ViewModel(['nickname' => $nickname]);
    }
} 