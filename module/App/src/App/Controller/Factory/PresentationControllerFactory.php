<?php
/**
 * Class PresenterControllerFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace App\Controller\Factory;

use App\Controller\PresentationController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PresentationControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $service   = $serviceLocator->get('app.service.presentation');
        $websocket = $serviceLocator->get('websocket.service');

        return new PresentationController($service, $websocket);
    }
} 