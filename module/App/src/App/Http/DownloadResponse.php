<?php
/**
 * Class DownloadResponse
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Http;

use Zend\Http\Response\Stream;

class DownloadResponse extends Stream
{
    /**
     * @var \SplFileInfo
     */
    private $fileInfo;

    public function __construct(\SplFileInfo $fileInfo, $realName)
    {
        $this->fileInfo = $fileInfo;
        $this->setStream(fopen($this->fileInfo->getRealPath(), 'r'));
        $this->setStreamName($this->fileInfo->getFilename());
        $this->getHeaders()->addHeaders(
            array(
                'Content-Disposition' => sprintf('attachment; filename="%s"', $realName),
                'Content-Type'        => mime_content_type($this->fileInfo->getRealPath()),
                'Content-Length'      => $this->fileInfo->getSize()
            )
        );
    }
}