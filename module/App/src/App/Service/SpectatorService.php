<?php
/**
 * Class SpectatorService
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Service;

use App\Entity\Spectator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class SpectatorService extends AbstractService
{
    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $objectRepository = $objectManager->getRepository(Spectator::class);
        parent::__construct($objectManager, $objectRepository);
    }

    /**
     * @param string $nickname
     * @return Spectator
     */
    public function findSpectatorByNickname($nickname)
    {
        return $this->repository->findOneBy(['nickname' => $nickname]);
    }
} 