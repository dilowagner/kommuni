<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Config\StandardConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;

class SessionService
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @param ServiceLocatorInterface $service
     */
    public function __construct(ServiceLocatorInterface $service)
    {
        $config = $service->get('Config');
        if (isset($config['session'])) {

            $session = $config['session'];
            $sessionConfig = null;
            if (isset($session['config'])) {
                $class   = isset($session['config']['class'])   ? $session['config']['class']   : 'Zend\Session\Config\SessionConfig';
                $options = isset($session['config']['options']) ? $session['config']['options'] : array();
                /** @var StandardConfig $sessionConfig */
                $sessionConfig = new $class();
                $sessionConfig->setOptions($options);
            }

            $sessionStorage = null;
            if (isset($session['storage'])) {
                $class = $session['storage'];
                $sessionStorage = new $class();
            }

            $sessionSaveHandler = null;
            if (isset($session['save_handler'])) {
                $sessionSaveHandler = $service->get($session['save_handler']);
            }
            $this->sessionManager = new SessionManager($sessionConfig, $sessionStorage, $sessionSaveHandler);

        } else {
            $this->sessionManager = new SessionManager();
        }
        Container::setDefaultManager($this->sessionManager);
    }

    /**
     * @return SessionManager
     */
    public function getSessionManager()
    {
        return $this->sessionManager;
    }
}