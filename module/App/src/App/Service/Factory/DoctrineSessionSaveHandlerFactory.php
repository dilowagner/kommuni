<?php
/**
 * @author Diego Wagner diegowagner4@gmail.com
 */
namespace App\Service\Factory;

use App\Session\DatabaseSessionConfig;
use Doctrine\ORM\EntityManager;
use App\Service\DoctrineSessionSaveHandler;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DoctrineSessionSaveHandlerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $objectManager */
        $objectManager = $serviceLocator->get(EntityManager::class);
        $connection = $objectManager->getConnection();
        $config = $serviceLocator->get('Config');
        $options = $config['websockets']['session']['db_options'];
        $databaseSessionConfig = new DatabaseSessionConfig($options);

        return new DoctrineSessionSaveHandler($connection, $databaseSessionConfig);
    }
}