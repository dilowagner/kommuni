<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 13/06/15
 * Time: 20:10
 */
namespace App\Service\Factory;

use App\Service\SessionService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SessionServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new SessionService($serviceLocator);
    }
}