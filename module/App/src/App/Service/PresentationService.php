<?php
/**
 * Class PresentationService
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace App\Service;

use Admin\Entity\Presentation;
use Admin\Entity\Report;
use Admin\Entity\ReportRegistry;
use Admin\Entity\User;
use Admin\Exception\BusinessException;
use Admin\Repository\PresentationRepository;
use App\Entity\Spectator;
use App\Repository\SpectatorRepository;
use Doctrine\Common\Persistence\ObjectManager;
use DOMPDFModule\View\Model\PdfModel;
use Zend\Mvc\View\Http\ViewManager;
use Zend\Stdlib\Parameters;

class PresentationService extends AbstractService
{
    /**
     * @var ViewManager
     */
    private $viewManager;

    /**
     * @param ObjectManager $objectManager
     * @param ViewManager $viewManager
     */
    public function __construct(ObjectManager $objectManager, ViewManager $viewManager)
    {
        $objectRepository = $objectManager->getRepository(Presentation::class);
        parent::__construct($objectManager, $objectRepository);
        $this->viewManager = $viewManager;
    }

    /**
     * Seta a apresentação como executável
     * @param Presentation $presentation
     * @param User $user
     * @return Presentation
     * @throws \Exception
     */
    public function startPresentation(Presentation $presentation, User $user)
    {
        $this->updatePresentationToNotRunnableByUser($user);
        $presentation->setRunnable(true);
        return parent::save($presentation);
    }

    /**
     * @param Presentation $presentation
     * @return bool
     * @throws \Exception
     */
    public function savePdf(Presentation $presentation)
    {
        // incluido o domínio no src das imagens
        $html = $presentation->getHtml();
        $html = str_replace('src="', 'src="'.BASE_URL, $html);
        $presentation->setHtml($html);

        $pdfModel = new PdfModel();
        $pdfModel->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdfModel->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
        //$pdfModel->setOption('filename', sprintf('%s.pdf', $presentation->getName()));
        $pdfModel->setTemplate('app/presentation/print.phtml');
        $pdfModel->setVariables([
            'presentation' => $presentation
        ]);

        $renderer = $this->viewManager->getRenderer();
        $html     = $renderer->render($pdfModel);

        $dompdf = new \DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();

        $name = sprintf('%s.pdf', $presentation->getName());
        $file = sprintf(Presentation::PATH_DOWNLOAD . '%s', $name);

        file_put_contents($file, $output);

        return true;
    }

    /**
     * @param User $user
     */
    public function updatePresentationToNotRunnableByUser(User $user)
    {
        /** @var PresentationRepository $repository */
        $repository = $this->repository;
        $repository->updatePresentationToNotRunnableByUser($user);
    }

    /**
     * @param Presentation $presentation
     * @param Parameters $params
     * @return Report
     * @throws \Exception
     */
    public function saveNewReport(Presentation $presentation, Parameters $params)
    {
        $report = new Report();
        $report->setLocal($params->local);
        $report->setAmountPeople($params->amountPeople);
        $report->setPresentation($presentation);
        return parent::save($report);
    }

    /**
     * @return array
     */
    public function findPresentation()
    {
        return $this->findOneBy(['runnable' => true]);
    }

    /**
     * @param $id
     * @return User
     * @throws BusinessException
     */
    public function findUser($id)
    {
        $repository = $this->objectManager->getRepository(User::class);
        $user = $repository->find($id);
        if(! ($user instanceof User)) {
            throw new BusinessException("Não foi possível recuperar os dados do usuário logado.");
        }
        return $user;
    }

    /**
     * @param $id
     * @return Report
     * @throws BusinessException
     */
    public function findReport($id)
    {
        $repository = $this->objectManager->getRepository(Report::class);
        $report = $repository->find($id);
        if(! ($report instanceof Report)) {
            throw new BusinessException(sprintf("Não foi possível recuperar os dados do relatório %s.", $id));
        }
        return $report;
    }

    /**
     * @param Report $report
     * @param Parameters $params
     * @return Report|object
     * @throws BusinessException
     */
    public function saveReportRegistry(Report $report, Parameters $params)
    {
        if(! $params->registry) {
            $registry = new ReportRegistry();
        } else {
            $repository = $this->objectManager->getRepository(ReportRegistry::class);
            $registry   = $repository->find($params->registry);
        }
        $registry->setReport($report);
        $registry->setType($params->type);
        $registry->setRegistry($params->slide);

        return parent::save($registry);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function clearAllSpectator()
    {
        try {
            /** @var SpectatorRepository $repository */
            $repository = $this->objectManager->getRepository(Spectator::class);
            return $repository->clearAll();
        } catch(\Exception $e) {
            throw $e;
        }
    }
} 