<?php
/**
 * @author Diego Wagner diegowagner4@gmail.com
 */
namespace App\Service;

use App\Session\DatabaseSessionConfig;
use Doctrine\DBAL\Connection;
use Zend\Serializer\Serializer;
use Zend\Session\SaveHandler\DbTableGateway;

class DoctrineSessionSaveHandler extends DbTableGateway implements \SessionHandlerInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @param Connection $connection
     * @param DatabaseSessionConfig $config
     */
    public function __construct(Connection $connection, DatabaseSessionConfig $config)
    {
        $this->connection = $connection;
        $this->options    = $config;
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return DatabaseSessionConfig
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * {@inheritdoc}
     */
    public function open($savePath, $sessionName)
    {
        $this->sessionSavePath = $savePath;
        $this->sessionName     = $sessionName;
        $this->lifetime        = ini_get('session.gc_maxlifetime');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function find($id)
    {
        $queryBuilder = $this->getConnection()->createQueryBuilder();
        return $queryBuilder->select('*')->from($this->getOptions()->getTableName(), 's')
                            ->where(
                                $queryBuilder->expr()->andX(
                                    $queryBuilder->expr()->eq($this->getOptions()->getIdColumn(), '?'),
                                    $queryBuilder->expr()->eq($this->getOptions()->getNameColumn(), '?')
                                )
                            )
                            ->setParameter(0, $id)
                            ->setParameter(1, $this->sessionName)
                            ->execute()
                            ->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * {@inheritDoc}
     */
    public function read($id)
    {
        $config = $this->getOptions();
        if ($row = $this->find($id)) {
            if ($row[$config->getModifiedColumn()] + $row[$config->getLifetimeColumn()] > time()) {
                return base64_decode($row[$config->getDataColumn()]);
            }
            $this->destroy($id);
        }
        return '';
    }

    /**
     * {@inheritDoc}
     */
    public function write($id, $data)
    {
//        $unserializedData = Serializer::unserialize($data);
//        var_dump($unserializedData);die;
//        $data = Serializer::unserialize($unserializedData->initialized);
        $data = array(
            $this->getOptions()->getModifiedColumn() => time(),
            $this->getOptions()->getDataColumn()     => base64_encode($data),
        );

        if ($this->find($id)) {
            return $this->getConnection()->update(
                $this->getOptions()->getTableName(),
                $data,
                array(
                    $this->getOptions()->getIdColumn()   => $id,
                    $this->getOptions()->getNameColumn() => $this->sessionName,
                )
            );
        }

        $data[$this->getOptions()->getLifetimeColumn()] = $this->lifetime;
        $data[$this->getOptions()->getIdColumn()]       = $id;
        $data[$this->getOptions()->getNameColumn()]     = $this->sessionName;
        $this->getConnection()->insert($this->getOptions()->getTableName(), $data);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function destroy($id)
    {
        return (bool) $this->getConnection()->delete($this->getOptions()->getTableName(), array(
            $this->getOptions()->getIdColumn()   => $id,
            $this->getOptions()->getNameColumn() => $this->sessionName,
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function gc($maxlifetime)
    {
        $queryBuilder = $this->getConnection()->createQueryBuilder();
        $queryBuilder->delete($this->getOptions()->getTableName())
            ->where(sprintf(
                '%s + %s < %s',
                $this->getConnection()->quoteIdentifier($this->options->getModifiedColumn()),
                $this->getConnection()->quoteIdentifier($this->options->getLifetimeColumn()),
                time()
            ));

        return $queryBuilder->execute();
    }

    /**
     * Returns a merge/upsert (i.e. insert or update) SQL query when supported by the database.
     *
     * @return string|null The SQL string or null when not supported
     */
    private function getMergeSql()
    {
        $config = $this->getOptions();
        return "INSERT INTO $config->getTableName() ($config->getIdColumn(), $config->getDataColumn(), $config->getTimeColumn(), $config->getLifetimeColumn(), $config->getModifiedColumn())".
               "VALUES (:id, :data, :time, :lifetime, :modified) ".
               "ON DUPLICATE KEY UPDATE $config->getDataColumn() = VALUES($config->getDataColumn()), $config->getTimeColumn() = VALUES($config->getTimeColumn())";
    }
}