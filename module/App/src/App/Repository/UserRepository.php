<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * Efetua o login do apresentador
     *
     * @method findByMailAndPassword($email, $password)
     * @param $mail
     * @param $password
     * @return bool
     * @author Diego Wagner <diegowagner4@gmail.com>
     * @access public
     * @throws \Exception
     */
    public function findByMailAndPassword($mail, $password)
    {
        try {
            /** @var \App\Entity\User $user */
            $user = $this->findOneBy(['mail' => $mail]);
            if (! is_null($user)) {
                $hashPassword = $user->encryptPassword($password);
                if (($user->getPassword() == $hashPassword)) {
                    return true;
                }
            }
            return false;
        } catch(\Exception $e) {
            throw $e;
        }
    }
}
