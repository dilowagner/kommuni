<?php
/**
 * Class PresentationRepository
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SpectatorRepository extends EntityRepository
{
    /**
     * Limpa a tabela de espectadores, para iniciar nova apresentação.
     * @return bool
     */
    public function clearAll()
    {
        $qb = $this->createQueryBuilder('e')
                   ->delete();
        $qb->getQuery()->execute();
        return true;
    }
}