<?php
/**
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace App\Enum;

abstract class SessionName
{
    const SESSION_NAME = 'kommuniapp';
}