<?php
return [
    'router' => [
        'routes' => [
            'app-index' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'app.controller.presentation',
                        'action'     => 'index'
                    ],
                ],
            ],
            'app-nickname' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/nickname',
                    'defaults' => [
                        'controller' => 'app.controller.nickname',
                        'action'     => 'index'
                    ],
                ],
            ],
            'app-presentation' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/presentation',
                    'defaults' => [
                        'controller' => 'app.controller.presentation',
                        'action'     => 'index',
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '[/:action[/:token[/:user]]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'defaults' => [
                                    'controller' => 'app.controller.presentation'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'app-print' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/print[/:id[/:token]]',
                    'defaults' => [
                        'controller' => 'app.controller.presentation',
                        'action'     => 'print',
                    ]
                ]
            ],
            'app-report' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/report',
                    'defaults' => [
                        'controller' => 'app.controller.presentation',
                        'action'     => 'save-report',
                    ]
                ]
            ],
            'app-slides' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/slides',
                    'defaults' => [
                        'controller' => 'app.controller.slides',
                        'action'     => 'index'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [

        ],
        'factories' => [
            'app.controller.nickname'     => 'App\Controller\Factory\NicknameControllerFactory',
            'app.controller.presentation' => 'App\Controller\Factory\PresentationControllerFactory',
        ]
    ],
    'service_manager' => [
        'factories' => [
            'app.service.spectator'   => 'App\Service\Factory\SpectatorServiceFactory',
            'app.service.presentation'=> 'App\Service\Factory\PresentationServiceFactory',
            'app.service.session'     => 'App\Service\Factory\SessionServiceFactory',
            'app.doctrine.session.save.handler' => 'App\Service\Factory\DoctrineSessionSaveHandlerFactory'
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'app.404',
        'exception_template'       => 'app.error',
        'template_map' => [
            'app.layout' => __DIR__ . '/../view/layout/layout.phtml',
            'app.index'  => __DIR__ . '/../view/app/index/index.phtml',
            'app.error'  => __DIR__ . '/../view/error/index.phtml',
            'app.404'    => __DIR__ . '/../view/error/404.phtml',
        ],
        'template_path_stack' => [
            'App' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ],
    'session' => [
        'config' => [
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => [
                'name' => \App\Enum\SessionName::SESSION_NAME,
            ],
        ],
        'storage'      => 'App\Session\DatabaseSessionStorage',
        'save_handler' => 'app.doctrine.session.save.handler',
    ]
];
