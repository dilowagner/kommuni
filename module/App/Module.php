<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ACL for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace App;

use Admin\Adapter\AuthAdapter;
use App\Enum\SessionName;
use App\Service\SessionService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Validator\AbstractValidator;
use Zend\Http\Request as HttpRequest;
use Zend\ServiceManager\ServiceLocatorInterface;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Bootstrap module
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $application  = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        //Tradutor de mensagens de erro do form
        $translator = $serviceManager->get('MvcTranslator');
        $translator->addTranslationFile(
            'phpArray', 'vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php', 'default'
        );
        $translator->addTranslationFile(
            'phpArray', 'vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Captcha.php', 'default'
        );
        AbstractValidator::setDefaultTranslator($translator);

        $request = $serviceManager->get('Request');
        if ($request instanceof HttpRequest) {
            $this->bootstrapSession($serviceManager);
        }
    }

    /**
     * @param ServiceLocatorInterface $serviceManager
     */
    public function bootstrapSession(ServiceLocatorInterface $serviceManager)
    {
        /** @var SessionService $sessionManager */
        $sessionManager = $serviceManager->get('app.service.session');
        $session = $sessionManager->getSessionManager();
        $session->start();

        $container = new Container(SessionName::SESSION_NAME);
        if (! isset($container->init)) {

            $request = $serviceManager->get('Request');
            $session->regenerateId(true);
            $container->init          = 1;
            $container->remoteAddr    = $request->getServer()->get('REMOTE_ADDR');
            $container->httpUserAgent = $request->getServer()->get('HTTP_USER_AGENT');

            $config = $serviceManager->get('Config');
            if (! isset($config['session'])) {
                return;
            }

            $sessionConfig = $config['session'];
            if (isset($sessionConfig['validators'])) {
                $chain   = $session->getValidatorChain();

                foreach ($sessionConfig['validators'] as $validator) {
                    switch ($validator) {
                        case 'Zend\Session\Validator\HttpUserAgent':
                            $validator = new $validator($container->httpUserAgent);
                            break;
                        case 'Zend\Session\Validator\RemoteAddr':
                            $validator  = new $validator($container->remoteAddr);
                            break;
                        default:
                            $validator = new $validator();
                    }
                    $chain->attach('session.validate', array($validator, 'isValid'));
                }
            }
        }
    }
}
