<?php
return [
    'websockets'    => [
        'session' => [
            'db_options' => [
                'db_table'        => 'session',
                'db_id_col'       => 'id',
                'db_data_col'     => 'data',
                'db_time_col'     => 'time',
                'db_lifetime_col' => 'lifetime'
            ]
        ],
        'server'    => [
            'host'          =>  'kommuni.com.br',
            'port'          =>  1000,
            'clients_limit' =>  30,
            'max_disconnections_time' =>  30,
            'verbose'	    =>  true,
            'encoding'		=>  'UTF-8',
            'log'			=>  false,
            'logfile'		=>  './data/logs/socket/actions.log',
            'action'		=>  '/websocket/open'
        ],
    ],
    'controllers' => [
        'factories' => [
            'server.controller.websocket'     => 'Server\Controller\Factory\WebsocketControllerFactory',
            'server.controller.websocket.CLI' => 'Server\Controller\Factory\WebsocketCLIControllerFactory',
        ],
    ],
    'router' => [
        'routes' => [
            'server-websocket' => [
                'type'    => 'Segment',
                'options' => [
                    'route'       => '/websocket[/:action]',
                    'constraints' => [
                        'action' => 'open',
                    ],
                    'defaults' => [
                        'controller' => 'server.controller.websocket',
                        'action'     => 'open',
                    ],
                ],
            ],
        ],
    ], 
    // Rotas de console
    'console' => [
        'router' => [
            'routes' => [
                'websocket-console' => [
                    'options'   => [
                        'route' => 'websocket open',
                        'defaults' => [
                            '__NAMESPACE__' => 'Server\Controller\WebsocketCLIController',
                            'controller'    => 'server.controller.websocket.CLI',
                            'action'        => 'open',
                        ],
                    ],
                ],  
                'websocket-console-info' => [
                    'options'   => [
                        'route' => 'websocket system [--verbose|-v] <option>',
                        'defaults' => [
                            '__NAMESPACE__' => 'Server\Controller\WebsocketCLIController',
                            'controller'    => 'server.controller.websocket.CLI',
                            'action'        => 'system',
                        ],
                    ],
                ],                
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'websocket.service' => 'Server\Service\Factory\WebsocketServiceFactory'
        ]
    ],
    'view_helpers' => [
        'invokables' => [
            'helper.socket' => 'Server\View\Helper\Socket',
        ]
    ]
];
