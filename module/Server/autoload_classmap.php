<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'Server\Module'                 => __DIR__ . '/Module.php',
    'ServerTest\Framework\TestCase' => __DIR__ . '/tests/Server/Framework/TestCase.php',
    'ServerTest\SampleTest'         => __DIR__ . '/tests/Server/SampleTest.php',
);
