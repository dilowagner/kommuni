<?php
namespace Server;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface,
    Zend\ModuleManager\Feature\ConfigProviderInterface,
    Zend\ModuleManager\Feature\ConsoleUsageProviderInterface,
    Zend\ModuleManager\Feature\ConsoleBannerProviderInterface,
    Zend\Console\Adapter\AdapterInterface as Console;

/**
 * Módulo para o lançamento do console WebSockets conexão permanente
 *
 * @package Zend Framework 2
 * @subpackage Server
 * @since PHP >=5.4
 * @version 1.0
 * @filesource /module/WebSockets/Module.php
 */
class Module implements 
            AutoloaderProviderInterface,
            ConfigProviderInterface,
            ConsoleUsageProviderInterface,
            ConsoleBannerProviderInterface {
       
    /**
     * Configurador boot do módulo Server
     * getConfig()
     * @access public
     * @return file
     */
    public function getConfig()
    {
        return include __DIR__.'/config/module.config.php';
    }

    /**
     * getAutoloaderConfig()
     * @access public
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            // install namespace for MVC application directory
            'Zend\Loader\ClassMapAutoloader'    =>  [
                __DIR__.'/autoload_classmap.php',
            ],            
            'Zend\Loader\StandardAutoloader'    =>  [
                'namespaces'    =>  [
                    __NAMESPACE__   =>  __DIR__.'/src/'.__NAMESPACE__,
                ],
            ],
        ];
    }
    
    /**
     * getConsoleUsage(Console $console)
     * @access public
     * @return console
     */
    public function getConsoleUsage(Console $console)
    {
        return [
            'websocket open ' => 'Websocket server start',
            'websocket system [--verbose|-v] <option>'   =>  'type the system command',
            ['option'          =>  'system command for your CLI'],
            ['--verbose|-v'    =>  '(optional) turn on verbose mode'],
        ];
    }

    /**
     * @param Console $console
     * @return string
     */
    public function getConsoleBanner(Console $console)
    {
        return '###################################'.PHP_EOL
              .'### Running WebSocket Module... ###'.PHP_EOL
              .'###################################';
    }
}
