<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;

/**
 * Mensagem de questão
 */
class QuestionMessage extends Message
{
    public $type = 'question';
    public $question;
    public $nickname;

    public function __construct(ConnectionInterface $connection, $data)
    {
        parent::__construct($connection, $data);

        $forbiddenWords = explode(', ', $data->forbiddenWords);
        $this->question = str_ireplace($forbiddenWords, '***', $data->question);
        $this->nickname = $this->connection->Session->get('nickname');
    }
}