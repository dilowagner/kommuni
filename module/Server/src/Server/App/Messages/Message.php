<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;

/**
 * Mensagem padrão
 */
abstract class Message implements MessageInterface
{
    /**
     * @var ConnectionInterface
    */
    protected $connection;

    /**
     * @var string JSON
    */
    protected $data;

    public function __construct(ConnectionInterface $connection, $data)
    {
        $this->connection = $connection;
        $this->data       = $data;
    }
}