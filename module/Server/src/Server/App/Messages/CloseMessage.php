<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;

class CloseMessage extends Message
{
    public $type = 'close';
    public $flag = false;

    public function __construct(ConnectionInterface $connection, $data)
    {
        parent::__construct($connection, $data);
        $this->flag = $data->flag;
    }
}