<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;
use Server\Exception\ExceptionStrategy;


/**
 * Gerenciador de mensagens, capaz de identificar o tipo da mensagem com base
 * nos seus campos
 */
class MessageManager
{
    /**
     * Cria um objeto que extende Message com base no tipo da mensagem
     *
     * @param string $json JSON recebido
     * @param ConnectionInterface $connection
     * @return Message ou classe que extende Message
     */
    public static function createMessage(ConnectionInterface $connection, $json)
    {
        $data    = json_decode($json);
        $message = null;

        $type = ucwords($data->type);
        $messageClass = sprintf('\\%s\\%sMessage', __NAMESPACE__ , $type);

        if(class_exists($messageClass)) {
            $message = new $messageClass($connection, $data);
        }

        if(! ($message instanceof MessageInterface)) {
            throw new ExceptionStrategy("Não foi possível encontrar a mensagem para realizar o processamento.");
        }
        return $message;
    }
}