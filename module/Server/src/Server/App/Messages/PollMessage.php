<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;

/**
 * Mensagem de votação de enquete
 */
class PollMessage extends Message
{
    public $type = 'poll';
    public $number;
    public $value;

    public function __construct(ConnectionInterface $connection, $data)
    {
        parent::__construct($connection, $data);

        $this->number = $data->number;
        $this->value  = $data->value;
    }
}