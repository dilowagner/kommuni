<?php
namespace Server\App\Messages;
use Server\App\Poll;

/**
 * Mensagem de resultado de enquete
 */
class PollResultMessage
{
    public $type = 'poll-result';
    public $number;
    public $votes;
    public $percentages;

    public function __construct(Poll $poll)
    {
        $this->number      = $poll->number;
        $this->votes       = $poll->getVotes();
        $this->percentages = $poll->getPercentages();
    }
}