<?php
namespace Server\App\Messages;

use Ratchet\ConnectionInterface;

/**
 * Mensagem de troca de slide
 */
class SlideMessage extends Message
{
    public $type = 'slide';
    public $indexh;
    public $indexv;
    public $indexf;
    public $nextindexh;
    public $nextindexv;
    protected $secret;
    protected $socketId;

    public function __construct(ConnectionInterface $connection, $data)
    {
        parent::__construct($connection, $data);
        $this->indexh     = $data->indexh;
        $this->indexv     = $data->indexv;
        $this->indexf     = $data->indexf;
        $this->nextindexh = $data->nextindexh;
        $this->nextindexv = $data->nextindexv;
        $this->secret     = $data->secret;
        $this->socketId   = $data->socketId;
    }
}