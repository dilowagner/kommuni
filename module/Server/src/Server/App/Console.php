<?php
/**
 * @author Diego Wagner <diego.wagner@toolsystems.com.br>
 */
namespace Server\App;

use Zend\Debug\Debug;

class Console
{
    /**
     * @var boolean
     */
    protected static $verbose = true;

    /**
     * @var string
     */
    protected static $encoding = 'UTF-8';

    /**
     * @return boolean
     */
    public static function isVerbose()
    {
        return self::$verbose;
    }

    /**
     * @param boolean $verbose
     */
    public static function setVerbose($verbose)
    {
        self::$verbose = $verbose;
    }

    /**
     * @param $data
     * @param bool $exit
     * @param null $encoding
     */
    public static function dump($data, $exit = false, $encoding = null)
    {
        // check if console is usable
        if(true === static::$verbose)
        {
            if(is_array($data)) {
                Debug::dump($data, date('[Y-m-d H:i:s] '));
            } else {
                if(!is_resource($data)) {
                    $encodingValue = is_null($encoding) ? static::$encoding : $encoding;
                    $data = mb_convert_encoding($data, $encodingValue);
                }
                $text = date('[Y-m-d H:i:s] ').$data."\r\n";
                echo $text;
            }
            if($exit) {
                die();
            }
        }
    }
} 