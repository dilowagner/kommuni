<?php
namespace Server\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,  
    Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Socket
 * ViewHelper para o módulo WebSockets
 * @package Server\View\Helper
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
class Socket extends AbstractHelper implements ServiceLocatorAwareInterface
{
    /**
     * Instância do Service Manager
     * @access protected
     * @var ServiceManager $sm
     */
    protected $sm;

    /**
     * @param ServiceLocatorInterface $serviceLocator 
     * @return CustomHelper 
     */  
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)  
    {  
        $this->sm = $serviceLocator;  
        return $this;  
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface 
     */  
    public function getServiceLocator()  
    {  
        return $this->sm;  
    }

    /**
     * @return $this
     */
    public function __invoke() {
	    return $this;
    }    
    
    /**
     * config($key)
     * @access public
     * @return string
     */
    public function config($key)
    {
        $config = $this->sm->getServiceLocator()->get('Configuration')['websockets']['server'];
        if(!isset($config[$key])) {
            return null;
        }
        return $config[$key];
    }
}