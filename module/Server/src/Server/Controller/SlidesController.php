<?php
/**
 * Class SlidesController
 * @author Diego Wagner <diegowagner4@gmail.com>
 */
namespace Server\Controller;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Server\App\Console;
use Server\App\Messages\CloseMessage;
use Server\App\Messages\MessageManager;
use Server\App\Messages\PollMessage;
use Server\App\Messages\PollResultMessage;
use Server\App\Messages\SlideMessage;
use Server\App\Poll;
use Server\App\Sender;

class SlidesController implements MessageComponentInterface
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @var \SplObjectStorage
     */
    protected $connections;

    /**
     * Última mensagem de troca de slide recebida
     * @var SlideMessage
     */
    protected $lastMessage;

    /**
     * Array das enquetes
     * @var Array de Poll
     */
    protected $polls;

    public function __construct(array $config)
    {
        $this->config      = $config;
        $this->connections = new \SplObjectStorage();
    }

    /**
     * Função executada quando o servidor recebe uma nova conexão
     *
     * @param ConnectionInterface $connection
     */
    public function onOpen(ConnectionInterface $connection)
    {
        //Quando um usuário é conectado, armazenamos sua conexão em um
        //SplObjectStorage
        $this->connections->attach($connection);
        //Se já houve uma troca de slides, envia para o usuário ir até o slide
        //atual
        if (isset($this->lastMessage)) {
            Sender::send($this->lastMessage, $connection);
        }
        //Captura o nickname definido na sessão
        $nickname = $connection->Session->get('nickname');

        Console::dump(sprintf("Spectator %s connected: %d", $nickname, $connection->resourceId));
        Console::dump(sprintf("Clients: %d / %d", count($this->connections), $this->config['server']['clients_limit']));
    }

    /**
     * Função executada quando a conexão é encerrada
     *
     * @param ConnectionInterface $connection
     */
    public function onClose(ConnectionInterface $connection)
    {
        //A conexão foi encerrada, remove do storage
        $this->connections->detach($connection);
        $nickname = $connection->Session->get('nickname');

        Console::dump(sprintf("Connection %s closed: %s", $nickname, $connection->resourceId));
        Console::dump(sprintf("Clients: %d / %d", count($this->connections), $this->config['server']['clients_limit']));
    }

    /**
     * Função executada quando ocorre algum erro com a conexão
     *
     * @param ConnectionInterface $connection
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $connection, \Exception $e)
    {
        $connection->close();
        Console::dump(sprintf("An error ocurred: %s", $e->getMessage()));
    }

    /**
     * Função executada quando o servidor recebe uma mensagem do socket
     *
     * @param ConnectionInterface $connection
     * @param String $jsonMessage
     */
    function onMessage(ConnectionInterface $connection, $jsonMessage)
    {
        //Envia a mensagem para o MessageManager, ele identificará qual o tipo
        //dela
        $message = MessageManager::createMessage($connection, $jsonMessage);

        if ($message instanceof SlideMessage) {
            $this->lastMessage = $message;
        } else if ($message instanceof PollMessage) {
            //Se é uma mensagem de enquete, inicia a enquete e computa o voto
            if (!isset($this->polls[$message->number])) {
                $this->polls[$message->number] = new Poll($message->number);
            }
            /** @var Poll $poll */
            $poll = $this->polls[$message->number];
            $poll->addVote($message->value);

            $message = new PollResultMessage($poll);
        }

        if ($message instanceof PollResultMessage) {
            // Envia para todos
            Console::dump('Broadcast to everybody');
            //Broadcast para todos
            foreach ($this->connections as $anotherConnection) {
                Sender::send($message, $anotherConnection);
            }
        } else {
            // Envia para os outros
            Console::dump('Broadcast to others');
            //Broadcast para todos, menos para quem enviou
            foreach ($this->connections as $anotherConnection) {
                if ($anotherConnection !== $connection) {
                    Sender::send($message, $anotherConnection);
                }
            }
        }

        if($message instanceof CloseMessage) {
            $this->finish($connection);
        } else {
            $nickname = $connection->Session->get('nickname');
            Console::dump($jsonMessage . ' - ' . $nickname);
        }
    }

    /**
     * Encerra todas as conexões do Socket...
     * @param ConnectionInterface $connection
     */
    private function finish(ConnectionInterface $connection)
    {
        foreach ($this->connections as $anotherConnection) {
            if ($anotherConnection !== $connection) {
                $this->onClose($anotherConnection);
            }
        }

        $this->onClose($connection);
        unset($this->lastMessage);
        $this->connections->removeAll($this->connections);
    }
} 