<?php
namespace Server\Controller; // Namespaces of current controller

use Server\Service\WebsocketService;
use Zend\Mvc\Controller\AbstractActionController;
use Server\Service\WebsocketService as Server;
use Server\Exception;

/**
 * Class WebsocketController
 * Controlador para ser executado através de um navegador
 * @package Server\Controller
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 * http://www.discoverytecnologia.com.br
 */
class WebsocketController extends AbstractActionController
{
    /**
     * @var WebsocketService
     */
    private $service;

    /**
     * @param Server $service
     */
    public function __construct(WebsocketService $service)
    {
        $this->service = $service;
    }
    
    /**
     * openAction() Running socket - server
     * @access public
     * @return console
     */    
    public function openAction()
    {
        // Tenta iniciar o servidor
        try {
	        // running server
            $this->service->run();
        } catch(Exception\ExceptionStrategy $e) {
            echo $e->getMessage();
        } 
    } 
}
