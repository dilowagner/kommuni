<?php
namespace Server\Controller;

use Server\Service\WebsocketService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;
use Server\Service\WebsocketService as Server;
use Server\Exception;

/**
 * Class WebsocketCLIController
 * Controlador para executar através de uma CL
 * @package Server\Controller
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 * http://www.discoverytecnologia.com.br
 */
class WebsocketCLIController extends AbstractActionController
{
    /**
     * @var WebsocketService
     */
    private $service;

    /**
     * @param Server $service
     */
    public function __construct(WebsocketService $service)
    {
        $this->service = $service;
    }

    /**
     * openAction()
     * @access public
     * @return console
     */    
    public function openAction()
    {   
        $request = $this->getRequest();
        if(!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('Use only for CLI!');
        }
        $this->service->run();
    } 
    
    /**
     * systemAction() System command
     * @access public
     * @return console
     */    
    public function systemAction()
    {   
        $request    = $this->getRequest();

        if(!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('Use only for CLI!');
        }
        
        // tenta iniciar as actions
        try {        
            // Get system service name  from console and check if the user used --verbose or -v flag
            $option     = $request->getParam('option', false);
            $verbose    = ($request->getParam('verbose')) ? $request->getParam('verbose') : $request->getParam('v');
            
            if($verbose) echo 'Command execute: '.$option.PHP_EOL;
            $option = preg_replace('#"#', '', $option);
            system($option, $val);

        } catch(Exception\ExceptionStrategy $e) {
            echo $e->throwMessage();
        }        
    }
}
