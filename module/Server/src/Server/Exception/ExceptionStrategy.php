<?php
/**
 * Class ExceptionStrategy
 *
 * Exception para o modulo Server
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Server\Exception;

class ExceptionStrategy extends \UnexpectedValueException
{
    /**
     * Somente mensagem em console
     * $message
     * @var type string
     * @access protected
     */
    protected $message = null;

    /**
     * Somente mensagem no Console
     * $line
     * @var type string
     * @access protected
     */
    protected $line = null;

    /**
     * @return string
     */
    public function throwMessage()
    {
	    return $this->getMessage().' [line: '.$this->getLine().']';
    }
}
