<?php
/**
 * Class ConnectionRefusedException
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Server\Exception;

class ConnectionRefusedException extends \Exception {}