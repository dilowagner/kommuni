<?php
namespace Server\Service;

use App\Enum\SessionName;
use App\Session\DatabaseSessionConfig;
use App\Session\SessionProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Ratchet\WebSocket\WsServer;
use Server\App\Console;
use Server\Controller\SlidesController;
use Server\Exception,
    Zend\Debug\Debug,
    Zend\Log\Logger,
    Zend\Log\Writer\Stream;
use Server\App\App;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\LegacyPdoSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

/**
 * Class WebsocketServer
 * Classe que provê o serviço de gerenciamento de conexão WebSocket
 *
 * @package Server\Service
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 * http://www.discoverytecnologia.com.br
 */
class WebsocketService
{
    /**
     * $config Server configuration
     * @access protected
     * @var  array
     */
    protected $config = null;
    
    /**
     * $logger Log object
     * @access protected
     * @var Logger $logger
     */
    protected $logger = null;
    
    /**
     * $log Log state
     * @access private
     * @var  boolean $log
     */
    private $log = false;
    
    /**
     * $current Current connection
     * @access protected
     * @var  resourse #id
     */
    protected $current = null;
    
    /**
     * $connections Connections array
     * @access protected
     * @var  array
     */
    protected $connections = array();
    
    /**
     * $clients clientes ativos
     * @access protected
     * @var  int
     */
    protected $clients = 0;

    /**
     * __construct(array $config) Initializes the settings
     * @param ObjectManager $objectManager
     * @param array $config array with the connection config
     * @throws \Exception
     */
    public function __construct(ObjectManager $objectManager, array $config)
    {
        if(empty($config)) {
            throw new Exception\ExceptionStrategy('Required parameters are incorrupted!');
        }

        $config = $this->attachParamsFromDatabaseConfig($objectManager, $config);

        $this->config = $config['websockets'];

        $serverConfig = $this->config['server'];
        // check if loging service is usable
        if(true === $serverConfig['log']) {
            // add log writer
            if(null === $this->logger) {
                if(!file_exists($serverConfig['logfile'])) {
                    throw new Exception\ExceptionStrategy("Error! File {$serverConfig['logfile']} does not exist");
                }

                $this->log = true;
                $this->logger = new Logger();
                $this->logger->addWriter(new Stream($serverConfig['logfile']));
            }
        }
    }

    /**
     * @param ObjectManager $objectManager
     * @param $config
     * @return mixed
     */
    private function attachParamsFromDatabaseConfig(ObjectManager $objectManager, $config)
    {
        /** @var Connection $connection */
        $connection = $objectManager->getConnection();
        $dsn = sprintf('%s:host=%s;dbname=%s;port=%s',
            $connection->getDriver()->getDatabasePlatform()->getName(),
            $connection->getHost(),
            $connection->getDatabase(),
            $connection->getPort()
        );

        $pdo = [
            'dsn'  => $dsn,
            'user' => $connection->getUsername(),
            'pass' => $connection->getPassword()
        ];
        $config['websockets']['pdo'] = $pdo;

        return $config;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        $serverConfig = $this->config['server'];
        return $serverConfig['host'];
    }

    /**
     * @return string
     */
    public function getWebsocketHost()
    {
        $serverConfig = $this->config['server'];
        return sprintf('%s:%s', $serverConfig['host'], $serverConfig['port']);
    }

    /**
     * run()
     * @access public
     * @return null
     */
    public function run()
    {
        // server config
        $serverConfig = $this->config['server'];
        $pdoConfig = $this->config['pdo'];
        $pdo = new \PDO($pdoConfig['dsn'], $pdoConfig['user'], $pdoConfig['pass']);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $sessionConfig = $this->config['session'];
        $databaseSessionConfig = new DatabaseSessionConfig($sessionConfig['db_options']);
        $sessionHandler = new LegacyPdoSessionHandler($pdo, $databaseSessionConfig->getOptions());
        $options = ['cookie_domain' => '.'.$serverConfig['host'], 'name' => SessionName::SESSION_NAME];
        $sessionProvider = new SessionProvider(new SlidesController($this->config), $sessionHandler, $options);
        $wsSlides = new WsServer($sessionProvider);

        $server = new App($serverConfig['host'], $serverConfig['port'], '0.0.0.0');
        $server->route('slides', $wsSlides, ['*']);

        Console::setVerbose($serverConfig['verbose']);
        Console::dump("Running server...");
        Console::dump(sprintf("Listening on: %s:%d", $serverConfig['host'], $serverConfig['port']));
        Console::dump(sprintf("Clients: %d / %d", $this->clients, $serverConfig['clients_limit']));
        $server->run();
    }
    
    /**
     * disconnect($connection)
     * @param $connection client
     * @access public
     * @return null
     */
    public function disconnect($connection)
    {
	    unset($connection);
    }
    
    /**
     * sendHandshake($connectionId, $headers) Do the handshaking between client and server
     * @param Resource $connectionId
     * @param string $headers response connection headers
     * @access protected
     * @return boolean
     */
    protected function sendHandshake($connectionId, $headers) 
    {
	    $this->console("Getting client WebSocket version...");
	    if(!preg_match("/Sec-WebSocket-Version: (.*)\r\n/", $headers, $match)) {
            $this->console("The client doesn't support WebSocket");
            return false;
        }
        $version = $match[1];
		
	    $this->console("Client WebSocket version is {$version}, (required: 13)");
        if($version != 13) {
            $this->console("WebSocket version 13 required (the client supports version {$version})");
            return false;
        }

        // Extract header variables
        if(preg_match("/GET (.*) HTTP/", $headers, $match))
            $root = $match[1];

        if(preg_match("/Host: (.*)\r\n/", $headers, $match))
            $host = $match[1];

        if(preg_match("/Origin: (.*)\r\n/", $headers, $match))
            $origin = $match[1];

        if(preg_match("/Sec-WebSocket-Key: (.*)\r\n/", $headers, $match))
            $key = $match[1];

        $this->console("Client headers are:");
        $this->console("\t- Root: ".$root);
        $this->console("\t- Host: ".$host);
        $this->console("\t- Origin: ".$origin);
        $this->console("\t- Sec-WebSocket-Key: ".$key);

        //$this->console("Generating Sec-WebSocket-Accept key...");
        $acceptKey = base64_encode(pack('H*', sha1($key. '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

        // setting up new response headers
        $upgrade =  [
            'HTTP/1.1 101 WebSocket Protocol Handshake',
            'Upgrade: websocket',
            'Connection: Upgrade',
            'WebSocket-Origin: '.$origin,
            'WebSocket-Location: ws://'.$host.$root,
            'Sec-WebSocket-Accept: '.$acceptKey
        ];
        $upgrade = implode("\r\n", $upgrade)."\r\n\r\n";

        //$this->console("Sending this response to the client {$connectionId}:\r\n".$upgrade);
        if(false === socket_write($connectionId, $upgrade, strlen($upgrade))) // use ====, because might be 0 bytes returned
        {
            $this->console("Error [".socket_last_error()."]: ".socket_strerror(socket_last_error()));
            // die() or callback from console
        } else {
            $this->console("Handshake is successfully done!");
        } 
        return true;
    }    

    /**
     * mask($text)
     * @param string $text message
     * @access protected
     * @return string
     */
    protected function mask($text)
    {
        $b1 = 0x80 | (0x1 & 0x0f);
        $header = '';
        $length = strlen($text);
        if($length <= 125) {
            $header = pack('CC', $b1, $length);
        } elseif($length > 125 && $length < 65536) {
            $header = pack('CCn', $b1, 126, $length);
        } elseif($length >= 65536) {
            $header = pack('CCNN', $b1, 127, $length);
        }
        return $header . $text;
    }
  
    /**
     * unmask($text)
     * @param string $text message
     * @access private
     * @return string
     */
    protected function unmask($text) 
    {
        $length = ord($text[1]) & 127;
        if($length == 126) {
            $masks = substr($text, 4, 4);
            $data  = substr($text, 8);
        } elseif($length == 127) {
            $masks = substr($text, 10, 4);
            $data  = substr($text, 14);
        } else {
            $masks = substr($text, 2, 4);
            $data  = substr($text, 6);
        }
        $text = "";
        for($i = 0; $i < strlen($data); ++$i) {
            $text .= $data[$i] ^ $masks[$i%4];
        }
        return $text;
    }
    
    /**
     * onMessage($msg)
     * @param string $msg message unencrypted
     * @access public
     * @return boolean
     */
    public function onMessage($msg, $socket)
    {
	    // criptografar dados recebidos
	    $msg = $this->mask($msg);
	    //foreach($this->_sockets as $sock)
	    //{
            @socket_write($socket, $msg, strlen($msg));
	    //}
	    return true;
    }

    /**
     * Saída do console
     * @param $data
     * @param bool $exception
     * @param bool $exit
     * @acceess public
     * @throws Exception\ExceptionStrategy
     */
    public function console($data, $exception = false, $exit = false)
    {
        $serverConfig = $this->config['server'];
        // check if console is usable
        if(true === $serverConfig['verbose'])
        {
            if(is_array($data))
            {
                Debug::dump($data, date('[Y-m-d H:i:s] '));
                if(isset($this->log)) {
                    $this->logger->info($data);
                }
            } else {

                if(!is_resource($data)) {
                    $data = mb_convert_encoding($data, $serverConfig['encoding']);
                }
                $text = date('[Y-m-d H:i:s] ').$data."\r\n";

                if($exception)
                {
                    if($this->log) {
                        $this->logger->crit($text);
                    }
                    throw new Exception\ExceptionStrategy($text);
                } else {
                    if($this->log) {
                        $this->logger->info($text);
                    }
                    echo $text;
                }
            }
            if($exit) {
                die();
            }
        }
    }
}
?>